﻿using SolCharge.Aruba.ETL.Contracts.Enums;
using SolCharge.Aruba.ETL.Core.Enums;

namespace SolCharge.Aruba.ETL.Core.Common
{
    public static class EnumConverter
    {
        public static ZoneEvent GetZoneEvent(ZoneEventEnum zoneEvent)
        {
            ZoneEvent @event;
            switch (zoneEvent)
            {
                case ZoneEventEnum.ZoneIn:
                    @event = ZoneEvent.ZoneIn;
                    break;
                case ZoneEventEnum.ZoneOut:
                    @event = ZoneEvent.ZoneOut;
                    break;
                default:
                    @event = ZoneEvent.ZoneIn;
                    break;
            }

            return @event;
        }

        public static TargetDevType GetTargetDevType(TargetDevTypeEnum devType)
        {
            TargetDevType targetDevType;

            switch (devType)
            {
                case TargetDevTypeEnum.TargetTypeUnknown:
                    targetDevType = TargetDevType.TargetTypeUnknown;
                    break;
                case TargetDevTypeEnum.TargetTypeStation:
                    targetDevType = TargetDevType.TargetTypeStation;
                    break;
                case TargetDevTypeEnum.TargetTypeTag:
                    targetDevType = TargetDevType.TargetTypeTag;
                    break;
                case TargetDevTypeEnum.TargetTypeUnsecure:
                    targetDevType = TargetDevType.TargetTypeUnsecure;
                    break;
                default:
                    targetDevType = TargetDevType.TargetTypeStation;
                    break;
            }
            return targetDevType;
        }

        public static ErrorCode GetErrorCode(ErrorCodeEnum error)
        {
            ErrorCode errorCode;
            switch (error)
            {
                case ErrorCodeEnum.ErrorCodeNoError:
                    errorCode = ErrorCode.ErrorCodeNoError;
                    break;
                case ErrorCodeEnum.ErrorCode0Rssi:
                    errorCode = ErrorCode.ErrorCode0Rssi;
                    break;
                case ErrorCodeEnum.ErrorCodeOnly1Rssi:
                    errorCode = ErrorCode.ErrorCodeOnly1Rssi;
                    break;
                case ErrorCodeEnum.ErrorCodeOnly2Rssi:
                    errorCode = ErrorCode.ErrorCodeOnly2Rssi;
                    break;
                case ErrorCodeEnum.ErrorCodeRssiQuality:
                    errorCode = ErrorCode.ErrorCodeRssiQuality;
                    break;
                case ErrorCodeEnum.ErrorCodeRssiOldTimestamp:
                    errorCode = ErrorCode.ErrorCodeRssiOldTimestamp;
                    break;
                case ErrorCodeEnum.ErrorCodeRssiCloseTimestamp:
                    errorCode = ErrorCode.ErrorCodeRssiCloseTimestamp;
                    break;
                case ErrorCodeEnum.ErrorCodeLegacy:
                    errorCode = ErrorCode.ErrorCodeLegacy;
                    break;
                default:
                    errorCode = ErrorCode.ErrorCodeNoError;
                    break;
            }

            return errorCode;
        }

        public static MeasurementUnit GetMeasurementUnit(MeasurementUnitEnum measurementUnit)
        {
            MeasurementUnit selectedMeasurement;

            switch (measurementUnit)
            {
                case MeasurementUnitEnum.Meters:
                    selectedMeasurement = MeasurementUnit.Meters;
                    break;
                case MeasurementUnitEnum.Feet:
                    selectedMeasurement = MeasurementUnit.Feet;
                    break;
                case MeasurementUnitEnum.Pixels:
                    selectedMeasurement = MeasurementUnit.Pixels;
                    break;
                default:
                    selectedMeasurement = MeasurementUnit.Meters;
                    break;
            }

            return selectedMeasurement;

        }

        public static Algorithm GetAlgorithm(AlgorithmEnum algorithm)
        {
            Algorithm selectedAlgorithm;

            switch (algorithm)
            {
                case AlgorithmEnum.AlgorithmApPlacement:
                    selectedAlgorithm = Algorithm.AlgorithmApPlacement;
                    break;
                case AlgorithmEnum.AlgorithmCalibration:
                    selectedAlgorithm = Algorithm.AlgorithmCalibration;
                    break;
                case AlgorithmEnum.AlgorithmEstimation:
                    selectedAlgorithm = Algorithm.AlgorithmEstimation;
                    break;
                case AlgorithmEnum.AlgorithmLowDensity:
                    selectedAlgorithm = Algorithm.AlgorithmLowDensity;
                    break;
                case AlgorithmEnum.AlgorithmTriangulation:
                    selectedAlgorithm = Algorithm.AlgorithmTriangulation;
                    break;
                default:
                    selectedAlgorithm = Algorithm.AlgorithmApPlacement;
                    break;
            }

            return selectedAlgorithm;
        }

        public static HtType GetHTType(HTTypeEnum type)
        {
            HtType selectedType;

            switch (type)
            {
                case HTTypeEnum.Htt20mz:
                    selectedType = HtType.Htt20mz;
                    break;
                case HTTypeEnum.Htt40mz:
                    selectedType = HtType.Htt40mz;
                    break;
                case HTTypeEnum.HttInvalid:
                    selectedType = HtType.HttInvalid;
                    break;
                case HTTypeEnum.HttNone:
                    selectedType = HtType.HttNone;
                    break;
                case HTTypeEnum.HttVht160mz:
                    selectedType = HtType.HttVht160mz;
                    break;
                case HTTypeEnum.HttVht20mz:
                    selectedType = HtType.HttVht20mz;
                    break;
                case HTTypeEnum.HttVht40mz:
                    selectedType = HtType.HttVht40mz;
                    break;
                case HTTypeEnum.HttVht80mz:
                    selectedType = HtType.HttVht80mz;
                    break;
                case HTTypeEnum.HttVht80plus80mz:
                    selectedType = HtType.HttVht80plus80mz;
                    break;
                default:
                    selectedType = HtType.Htt20mz;
                    break;
            }

            return selectedType;
        }

        public static RadioMode GetRadioMode(RadioModeEnum mode)
        {
            RadioMode selectedMode;

            switch (mode)
            {
                case RadioModeEnum.RadioModeAirMonitor:
                    selectedMode = RadioMode.RadioModeAirMonitor;
                    break;
                case RadioModeEnum.RadioModeAp:
                    selectedMode = RadioMode.RadioModeAp;
                    break;
                case RadioModeEnum.RadioModeMeshPoint:
                    selectedMode = RadioMode.RadioModeMeshPoint;
                    break;
                case RadioModeEnum.RadioModeMeshPortal:
                    selectedMode = RadioMode.RadioModeMeshPortal;
                    break;
                case RadioModeEnum.RadioModeSpectrumSensor:
                    selectedMode = RadioMode.RadioModeSpectrumSensor;
                    break;
                case RadioModeEnum.RadioModeUnknown:
                    selectedMode = RadioMode.RadioModeUnknown;
                    break;
                default:
                    selectedMode = RadioMode.RadioModeAirMonitor;
                    break;
            }

            return selectedMode;
        }

        public static PhyType GetPhyType(PhyTypeEnum phyType)
        {
            PhyType selectedType;

            switch (phyType)
            {
                case PhyTypeEnum.PhyType80211a:
                    selectedType = PhyType.PhyType80211a;
                    break;
                case PhyTypeEnum.PhyType80211ag:
                    selectedType = PhyType.PhyType80211ag;
                    break;
                case PhyTypeEnum.PhyType80211b:
                    selectedType = PhyType.PhyType80211b;
                    break;
                case PhyTypeEnum.PhyType80211g:
                    selectedType = PhyType.PhyType80211g;
                    break;
                case PhyTypeEnum.PhyTypeInvalid:
                    selectedType = PhyType.PhyTypeInvalid;
                    break;
                default:
                    selectedType = PhyType.PhyType80211a;
                    break;
            }

            return selectedType;
        }
    }
}
