﻿using System;

namespace SolCharge.Aruba.ETL.Core.Common
{
    public interface IDateTime
    {
        DateTimeOffset CreatedAt { get; set; }
        DateTimeOffset? ModifiedAt { get; set; }
    }
}
