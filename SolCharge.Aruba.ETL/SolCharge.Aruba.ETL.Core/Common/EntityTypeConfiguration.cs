﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Common
{
    public abstract class EntityTypeConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : Entity, IDateTime
    {
        public abstract void Configure(EntityTypeBuilder<TEntity> builder);

        public void ConfigureAuditFields(EntityTypeBuilder<TEntity> builder)
        {
            builder.Property(x => x.CreatedAt).IsRequired();
            builder.Property(x => x.ModifiedAt);
        }
    }
}
