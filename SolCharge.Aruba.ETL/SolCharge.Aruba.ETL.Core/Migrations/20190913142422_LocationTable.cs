﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SolCharge.Aruba.ETL.Core.Migrations
{
    public partial class LocationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModifiedAt = table.Column<DateTimeOffset>(nullable: true),
                    StaEthMac = table.Column<string>(nullable: true),
                    StaLocationX = table.Column<double>(nullable: false),
                    StaLocationY = table.Column<double>(nullable: false),
                    ErrorLevel = table.Column<long>(nullable: false),
                    Associated = table.Column<bool>(nullable: false),
                    CampusId = table.Column<string>(nullable: true),
                    BuildingId = table.Column<string>(nullable: true),
                    FloorId = table.Column<string>(nullable: true),
                    HashedStaEthMac = table.Column<string>(nullable: true),
                    LocAlgorithm = table.Column<int>(nullable: false),
                    RssiVal = table.Column<long>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Altitude = table.Column<double>(nullable: false),
                    Unit = table.Column<int>(nullable: false),
                    TargetType = table.Column<int>(nullable: false),
                    ErrorCode = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Location");
        }
    }
}
