﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SolCharge.Aruba.ETL.Core.Migrations
{
    public partial class GeoFenceNotifyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GeoFenceNotify",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModifiedAt = table.Column<DateTimeOffset>(nullable: true),
                    GeofenceEvent = table.Column<int>(nullable: false),
                    GeofenceId = table.Column<string>(nullable: true),
                    GeofenceName = table.Column<string>(nullable: true),
                    StaMac = table.Column<string>(nullable: true),
                    Associated = table.Column<bool>(nullable: false),
                    DwellTime = table.Column<long>(nullable: false),
                    HashedStaMac = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeoFenceNotify", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GeoFenceNotify");
        }
    }
}
