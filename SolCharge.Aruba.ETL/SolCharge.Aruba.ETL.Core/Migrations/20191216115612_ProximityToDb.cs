﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SolCharge.Aruba.ETL.Core.Migrations
{
    public partial class ProximityToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Proximity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModifiedAt = table.Column<DateTimeOffset>(nullable: true),
                    StaEthMac = table.Column<string>(nullable: true),
                    RadioMac = table.Column<string>(nullable: true),
                    RssiVal = table.Column<int>(nullable: false),
                    ApName = table.Column<string>(nullable: true),
                    HashedStaEthMac = table.Column<string>(nullable: true),
                    TargetDevType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proximity", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Proximity");
        }
    }
}
