﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SolCharge.Aruba.ETL.Core.Migrations
{
    public partial class FloorTableToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Floor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModifiedAt = table.Column<DateTimeOffset>(nullable: true),
                    FloorId = table.Column<string>(nullable: true),
                    FloorName = table.Column<string>(nullable: true),
                    FloorLatitude = table.Column<double>(nullable: false),
                    FloorLongitude = table.Column<double>(nullable: false),
                    FloorImagePath = table.Column<string>(nullable: true),
                    FloorImageWidth = table.Column<double>(nullable: false),
                    FloorImageLength = table.Column<double>(nullable: false),
                    BuildingId = table.Column<string>(nullable: true),
                    FloorLevel = table.Column<double>(nullable: false),
                    Units = table.Column<string>(nullable: true),
                    GridSize = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Floor", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Floor");
        }
    }
}
