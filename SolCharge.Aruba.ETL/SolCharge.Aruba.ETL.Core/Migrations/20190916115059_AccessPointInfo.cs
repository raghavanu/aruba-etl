﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SolCharge.Aruba.ETL.Core.Migrations
{
    public partial class AccessPointInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccessPointInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModifiedAt = table.Column<DateTimeOffset>(nullable: true),
                    ApMac = table.Column<string>(nullable: true),
                    ApName = table.Column<string>(nullable: true),
                    RadioBssid = table.Column<string>(nullable: true),
                    RssiVal = table.Column<long>(nullable: false),
                    GeoFenceNotifyEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessPointInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccessPointInfo_GeoFenceNotify_GeoFenceNotifyEntityId",
                        column: x => x.GeoFenceNotifyEntityId,
                        principalTable: "GeoFenceNotify",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccessPointInfo_GeoFenceNotifyEntityId",
                table: "AccessPointInfo",
                column: "GeoFenceNotifyEntityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccessPointInfo");
        }
    }
}
