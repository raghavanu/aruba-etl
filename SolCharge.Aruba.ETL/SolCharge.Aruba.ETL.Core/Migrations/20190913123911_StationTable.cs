﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SolCharge.Aruba.ETL.Core.Migrations
{
    public partial class StationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Station",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModifiedAt = table.Column<DateTimeOffset>(nullable: true),
                    StaEthMac = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true),
                    Bssid = table.Column<string>(nullable: true),
                    DeviceType = table.Column<string>(nullable: true),
                    StaIpAddress = table.Column<string>(nullable: true),
                    HashedStaEthMac = table.Column<string>(nullable: true),
                    HashedStaIpAddress = table.Column<string>(nullable: true),
                    Vlan = table.Column<long>(nullable: false),
                    Ht = table.Column<int>(nullable: false),
                    ApName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Station", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Station");
        }
    }
}
