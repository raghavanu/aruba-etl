﻿namespace SolCharge.Aruba.ETL.Core.Extensions
{
    public static class ByteToStringConverter
    {
        public static string ConvertToMacAddress(this byte[] byteArray)
        {
            var macAddress = "";
            for (int i = 0; i < byteArray.Length; i++)
            {
                // Display the physical address in hexadecimal.
                macAddress += byteArray[i].ToString("X2");
                // Insert a hyphen after each byte, unless we are at the end of the 
                // address.
                if (i != byteArray.Length - 1)
                {
                    macAddress += ":";
                }
            }

            return macAddress;
        }

        public static string ConvertToIpAddress(this byte[] byteArray)
        {
            var ipAddress = "";
            for (int i = 0; i < byteArray.Length; i++)
            {
                // Display the physical address in hexadecimal.
                ipAddress += byteArray[i].ToString("X2");
                // Insert a hyphen after each byte, unless we are at the end of the 
                // address.
                if (i != byteArray.Length - 1)
                {
                    ipAddress += ".";
                }
            }

            return ipAddress;
        }

        public static string ConvertToString(this byte[] byteArray)
        {
            var stringValue = "";
            for (int i = 0; i < byteArray.Length; i++)
            {
                // Display the physical address in hexadecimal.
                stringValue += byteArray[i].ToString("X2");
                // Insert a hyphen after each byte, unless we are at the end of the 
                // address.
                if (i != byteArray.Length - 1)
                {
                    stringValue += "";
                }
            }

            return stringValue;
        }
    }
}
