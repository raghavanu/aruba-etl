﻿using System.IO;

namespace SolCharge.Aruba.ETL.Core.Extensions
{
    public static class MemoryStreamExtension
    {
        public static void WriteAndResetPosition(this MemoryStream stream, byte[] data, int offset, int count)
        {
            stream.Write(data, offset, count);
            stream.Position -= count;
        }
    }
}
