﻿using Microsoft.EntityFrameworkCore;
using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Entities;
using SolCharge.Aruba.ETL.Core.Infrastructure;
using System.Collections.Generic;

namespace SolCharge.Aruba.ETL.Core.Jobs
{
    public class GeoFenceNotifyJob : Job
    {
        static object _lock = new object();

        private List<GeoFenceNotifyEntity> _entities = new List<GeoFenceNotifyEntity>();

        public override void Transform(NBEventModel eventModel)
        {
            GeoFenceNotifyEntity entity = eventModel.GeofenceNotify;

            _entities.Add(entity);

            CanWrite(_entities, _lock, () =>
            {
                var connectionstring = "Data Source=solcharge.database.windows.net;Initial Catalog=solcharge-aruba;Persist Security Info=False;User ID=solchargeadmin;Password=Solcharge@1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=300";

                var optionsBuilder = new DbContextOptionsBuilder<SolChargeArubaDbContext>();
                optionsBuilder.UseSqlServer(connectionstring);

                using (var context = new SolChargeArubaDbContext(optionsBuilder.Options))
                {
                    context.AddRange(_entities);
                    context.SaveChanges();
                }
            });

        }
    }
}
