﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace SolCharge.Aruba.ETL.Core
{
    public class SolChargeArubaDbContextFactory : IDesignTimeDbContextFactory<SolChargeArubaDbContext>
    {
        public SolChargeArubaDbContext CreateNewInstance()
        {
            return CreateDbContext(null);
        }

        public SolChargeArubaDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SolChargeArubaDbContext>();
            optionsBuilder.UseSqlServer("Data Source=solcharge.database.windows.net;Initial Catalog=solcharge-aruba;Persist Security Info=False;User ID=solchargeadmin;Password=Solcharge@1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=300");

            return new SolChargeArubaDbContext(optionsBuilder.Options);
        }
    }
}
