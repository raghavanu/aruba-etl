﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Entities;

namespace SolCharge.Aruba.ETL.Core.Configuration
{
    public class PresenceEntityConfiguration : EntityTypeConfiguration<PresenceEntity>
    {
        public override void Configure(EntityTypeBuilder<PresenceEntity> builder)
        {
            builder.ToTable("Presence");

            ConfigureAuditFields(builder);
        }
    }
}
