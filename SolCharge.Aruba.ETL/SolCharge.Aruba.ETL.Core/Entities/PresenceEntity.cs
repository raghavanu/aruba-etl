﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Enums;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class PresenceEntity : Entity
    {
        public string MacAddress { get; set; }
        public bool Associated { get; set; }
        public string HashedStaEthMac { get; set; }
        public string ApName { get; set; }
        public string RadioMac { get; set; }
        public TargetDevType TargetType { get; set; }

        public static implicit operator PresenceEntity(PresenceModel model)
        {
            if (model == null)
                return null;

            return new PresenceEntity
            {
                MacAddress = model.StaEthMac.Addr.ConvertToMacAddress(),
                ApName = model.ApName,
                HashedStaEthMac = model.HashedStaEthMac.ConvertToMacAddress(),
                Associated = model.Associated,
                RadioMac = model.RadioMac.Addr.ConvertToMacAddress(),
                TargetType = EnumConverter.GetTargetDevType(model.TargetType)
            };
        }
    }
}
