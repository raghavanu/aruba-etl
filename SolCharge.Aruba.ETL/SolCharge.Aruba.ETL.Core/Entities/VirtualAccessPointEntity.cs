﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class VirtualAccessPointEntity : Entity
    {
        public string Bssid { get; set; }
        public string Ssid { get; set; }
        public string RadioBssid { get; set; }

        public static implicit operator VirtualAccessPointEntity(VirtualAccessPointModel model)
        {
            if (model == null)
                return null;

            return new VirtualAccessPointEntity
            {
                Bssid = model.Bssid.Addr.ToString(),
                Ssid = model.Ssid,
                RadioBssid = model.RadioBssid.Addr.ToString()
            };
        }
    }
}
