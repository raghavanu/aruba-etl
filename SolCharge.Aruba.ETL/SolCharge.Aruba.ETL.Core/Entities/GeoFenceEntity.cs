﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;
using System.Linq;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class GeoFenceEntity: Entity
    {
        public string FloorId { get; set; }
        public string GeoFenceId { get; set; }
        public string GeoFenceName { get; set; }
        public string Type { get; set; }
        public string XAxisArray { get; set; }
        public string YAxisArray { get; set; }


        public static implicit operator GeoFenceEntity(GeoFenceModel model)
        {
            if (model == null)
                return null;

            return new GeoFenceEntity
            {
                FloorId = model.FloorId.ConvertToString(),
                GeoFenceId = model.FloorId.ConvertToString(),
                GeoFenceName = model.GeofenceName,
                Type = model.Type,
                XAxisArray = string.Join(",", model.PointLists.Select(x => x.X.ToString()).ToArray()),
                YAxisArray = string.Join(",", model.PointLists.Select(x => x.Y.ToString()).ToArray()),
            };
        }
    }
}
