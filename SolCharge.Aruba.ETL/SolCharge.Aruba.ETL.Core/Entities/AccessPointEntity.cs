﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;
using System;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    //public class AccessPointEntity : Entity
    //{
    //    public string ApEthMac { get; set; }
    //    public string ApName { get; set; }
    //    public string ApGroup { get; set; }
    //    public string ApModel { get; set; }
    //    public string DeploymentMode { get; set; }
    //    public string ApIpAddressFamily { get; set; }
    //    public string ApIpAddress { get; set; }
    //    public long Reboots { get; set; }
    //    public long RebootStraps { get; set; }
    //    public string ManagedByAddressFamily { get; set; }
    //    public string ManagedByAddress { get; set; }
    //    public string ManagedByKey { get; set; }
    //    public string RadioBssidAddress { get; set; }
    //    public bool IsMaster { get; set; }
    //    public string CampusId { get; set; }
    //    public string BuildingId { get; set; }
    //    public string FloorId { get; set; }
    //    public double Latitude { get; set; }
    //    public double Longitude { get; set; }
    //    public double Xaxis { get; set; }
    //    public double Yaxis { get; set; }
    //    public int TimeStamp { get; set; }




    //    public static implicit operator AccessPointEntity(AccessPointModel model)
    //    {
    //        if (model == null)
    //            return null;

    //        return new AccessPointEntity
    //        {
    //            ApEthMac = model.ApEthMac.Addr.ConvertToMacAddress(),
    //            ApName = model.ApName,
    //            ApGroup = model.ApGroup,
    //            ApModel = model.ApModel,
    //            //DeploymentMode = model.DeplMode,
    //            ApIpAddressFamily = model.ApIpAddress.Af.ToString(), /// TODO: need to check this
    //            ApIpAddress = model.ApIpAddress.Addr.ConvertToIpAddress(),
    //            Reboots = Convert.ToInt64(model.Reboots),
    //            RebootStraps = Convert.ToInt64(model.Rebootstraps),
    //            ManagedByAddress = model.ManagedBy.Addr.ConvertToMacAddress(),
    //            ManagedByKey = model.ManagedBy.Af.ToString(), /// TODO: need to check this


    //        };
    //    }
    //}
}
