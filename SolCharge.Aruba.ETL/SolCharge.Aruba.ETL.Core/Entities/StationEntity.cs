﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Enums;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class StationEntity : Entity
    {
        public string StaEthMac { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string Bssid { get; set; }
        public string DeviceType { get; set; }
        public string StaIpAddress { get; set; }
        public string HashedStaEthMac { get; set; }
        public string HashedStaIpAddress { get; set; }
        public long Vlan { get; set; }
        public HtType Ht { get; set; }
        public string ApName { get; set; }

        public static implicit operator StationEntity(StationModel model)
        {
            if (model == null)
                return null;

            return new StationEntity
            {
                StaEthMac = model.StaEthMac.Addr.ConvertToMacAddress(),
                Username = model.Username,
                Role = model.Role,
                Bssid = model.Bssid.Addr.ConvertToMacAddress(),
                DeviceType = model.DeviceType,
                StaIpAddress = model.StaIpAddress.Addr.ConvertToIpAddress(),
                HashedStaEthMac = model.HashedStaEthMac.ConvertToMacAddress(),
                HashedStaIpAddress = model.HashedStaIpAddress.ConvertToIpAddress(),
                ApName = model.ApName,
                Vlan = model.Vlan,
                Ht = EnumConverter.GetHTType(model.Ht)
            };
        }
    }
}
