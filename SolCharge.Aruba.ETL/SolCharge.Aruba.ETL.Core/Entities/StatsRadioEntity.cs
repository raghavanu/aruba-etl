﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Enums;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public partial class StatsRadioEntity : Entity
    {
        public string ApEthMac { get; set; }
        public long RadioNumber { get; set; }
        public long Channel { get; set; }
        public PhyType Phy { get; set; }
        public RadioMode Mode { get; set; }
        public long NoiseFloor { get; set; }
        public long TxPower { get; set; }
        public long ChannelUtilization { get; set; }
        public long RxChannelUtilization { get; set; }
        public long TxChannelUtilization { get; set; }
        public long TxReceived { get; set; }
        public long TxTransmitted { get; set; }
        public long TxDropped { get; set; }
        public long TxDataReceived { get; set; }
        public long TxDataTransmitted { get; set; }
        public long TxDataRetried { get; set; }
        public long RxFrames { get; set; }
        public long RxRetried { get; set; }
        public long RxDataFrames { get; set; }
        public long RxDataRetried { get; set; }
        public long RxFrameErrors { get; set; }
        public long ActualEirp { get; set; }
        public string RadioMac { get; set; }
        public long TxDataBytes { get; set; }
        public long RxDataBytes { get; set; }
        public long RadioBand { get; set; }
        public long ChannelBusy64 { get; set; }
        public long StaNumber { get; set; }

        public static implicit operator StatsRadioEntity(StatsRadioModel model)
        {
            if (model == null)
                return null;

            return new StatsRadioEntity
            {
                ApEthMac = model.ApEthMac.Addr.ToString(),
                RadioNumber = model.RadioNumber,
                Channel = model.Channel,
                Phy = EnumConverter.GetPhyType(model.Phy),
                Mode = EnumConverter.GetRadioMode(model.Mode),
                NoiseFloor = model.NoiseFloor,
                TxPower = model.TxPower,
                ChannelUtilization = model.ChannelUtilization,
                RxChannelUtilization = model.RxChannelUtilization,
                TxChannelUtilization = model.TxChannelUtilization,
                TxReceived = model.TxReceived,
                TxTransmitted = model.TxTransmitted,
                TxDropped = model.TxDropped,
                TxDataReceived = model.TxDataReceived,
                TxDataTransmitted = model.TxDataTransmitted,
                TxDataRetried = model.TxDataRetried,
                RxFrames = model.RxFrames,
                RxRetried = model.RxRetried,
                RxDataFrames = model.RxDataFrames,
                RxDataRetried = model.RxDataRetried,
                RxFrameErrors = model.RxFrameErrors,
                ActualEirp = model.ActualEirp,
                RadioMac = model.RadioMac.Addr.ToString(),
                ChannelBusy64 = model.ChannelBusy64,
                StaNumber = model.StaNumber
            };
        }
    }
}
