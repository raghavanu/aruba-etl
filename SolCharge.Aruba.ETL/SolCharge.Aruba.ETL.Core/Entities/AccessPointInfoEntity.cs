﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class AccessPointInfoEntity : Entity
    {
        public string ApMac { get; set; }
        public string ApName { get; set; }
        public string RadioBssid { get; set; }
        public long RssiVal { get; set; }

        public static implicit operator AccessPointInfoEntity(AccessPointInfoModel model)
        {
            if (model == null)
                return null;

            return new AccessPointInfoEntity
            {
                ApMac = model.ApMac.Addr.ConvertToMacAddress(),
                ApName = model.ApName,
                RadioBssid = model.RadioBssid.Addr.ConvertToMacAddress(),
                RssiVal = model.RssiVal
            };
        }
    }
}
