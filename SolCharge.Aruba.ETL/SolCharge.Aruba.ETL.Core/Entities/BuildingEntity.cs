﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class BuildingEntity : Entity
    {
        public string BuildingId { get; set; }
        public string BuildingName { get; set; }
        public string CampusId { get; set; }

        public static implicit operator BuildingEntity(BuildingModel model)
        {
            if (model == null)
                return null;

            return new BuildingEntity
            {
                BuildingId = model.BuildingId.ConvertToString(),
                BuildingName = model.BuildingName,
                CampusId = model.CampusId.ConvertToString()
            };
        }
    }
}
