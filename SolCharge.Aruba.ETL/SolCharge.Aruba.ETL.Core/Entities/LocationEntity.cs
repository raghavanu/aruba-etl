﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Enums;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;
using System;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class LocationEntity : Entity
    {
        public string StaEthMac { get; set; }
        public double StaLocationX { get; set; }
        public double StaLocationY { get; set; }
        public long ErrorLevel { get; set; }
        public bool Associated { get; set; }
        public string CampusId { get; set; }
        public string BuildingId { get; set; }
        public string FloorId { get; set; }
        public string HashedStaEthMac { get; set; }
        public Algorithm LocAlgorithm { get; set; }
        public long RssiVal { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public MeasurementUnit Unit { get; set; }
        public TargetDevType TargetType { get; set; }
        public ErrorCode ErrorCode { get; set; }
        //public List<string> GeofenceIds { get; set; }

        public static implicit operator LocationEntity(LocationModel model)
        {
            if (model == null)
                return null;

            return new LocationEntity
            {
                StaEthMac = model.StaEthMac.Addr.ConvertToMacAddress(),
                StaLocationX = model.StaLocationX,
                StaLocationY = model.StaLocationY,
                ErrorLevel = Convert.ToInt64(model.ErrorLevel),
                Associated = model.Associated,
                CampusId = model.CampusId.ConvertToString(),
                BuildingId = model.BuildingId.ConvertToString(),
                FloorId = model.FloorId.ConvertToString(),
                HashedStaEthMac = model.HashedStaEthMac.ConvertToMacAddress(),
                RssiVal = model.RssiVal,
                Latitude = model.Latitude,
                Longitude = model.Longitude,
                TargetType = EnumConverter.GetTargetDevType(model.TargetType),
                ErrorCode = EnumConverter.GetErrorCode(model.ErrCode),
                Unit = EnumConverter.GetMeasurementUnit(model.Unit),
                LocAlgorithm = EnumConverter.GetAlgorithm(model.LocAlgorithm),
                Altitude = model.Altitude
            };
        }
    }
}
