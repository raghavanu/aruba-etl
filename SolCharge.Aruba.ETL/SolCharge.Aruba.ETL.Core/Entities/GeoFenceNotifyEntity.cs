﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Enums;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class GeoFenceNotifyEntity : Entity
    {
        public ZoneEvent GeofenceEvent { get; set; }
        public string GeofenceId { get; set; }
        public string GeofenceName { get; set; }
        public string StaMac { get; set; }
        public bool Associated { get; set; }
        public long DwellTime { get; set; }
        public string HashedStaMac { get; set; }

        public List<AccessPointInfoEntity> AccessPointInfos { get; set; }

        public static implicit operator GeoFenceNotifyEntity(GeoFenceNotifyModel model)
        {
            if (model == null)
                return null;

            return new GeoFenceNotifyEntity
            {
                GeofenceEvent = EnumConverter.GetZoneEvent(model.GeofenceEvent),
                GeofenceId = model.GeofenceId.ConvertToString(),
                GeofenceName = model.GeofenceName,
                StaMac = model.StaMac.Addr.ConvertToMacAddress(),
                Associated = model.Associated,
                DwellTime = model.DwellTime,
                HashedStaMac = model.HashedStaMac.ConvertToMacAddress(),
                AccessPointInfos = model.AccessPointInfoes.Select(x => new AccessPointInfoEntity
                {
                    ApMac = x.ApMac.Addr.ConvertToMacAddress(),
                    ApName = x.ApName,
                    RadioBssid = x.RadioBssid.Addr.ConvertToMacAddress(),
                    RssiVal = x.RssiVal
                }).ToList()
            };
        }
    }
}
