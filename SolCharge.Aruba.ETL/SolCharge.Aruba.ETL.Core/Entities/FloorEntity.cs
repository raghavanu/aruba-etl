﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class FloorEntity : Entity
    {
        public string FloorId { get; set; }
        public string FloorName { get; set; }
        public double FloorLatitude { get; set; }
        public double FloorLongitude { get; set; }
        public string FloorImagePath { get; set; }
        public double FloorImageWidth { get; set; }
        public double FloorImageLength { get; set; }
        public string BuildingId { get; set; }
        public double FloorLevel { get; set; }
        public string Units { get; set; }
        public double GridSize { get; set; }

        public static implicit operator FloorEntity(FloorModel model)
        {
            if (model == null)
                return null;

            return new FloorEntity
            {
                FloorId = model.FloorId.ConvertToMacAddress(),
                FloorName = model.FloorName,
                FloorLatitude = model.FloorLatitude,
                FloorLongitude = model.FloorLongitude,
                FloorImagePath = model.FloorImgPath,
                FloorImageWidth = model.FloorImgWidth,
                FloorImageLength = model.FloorImgLength,
                BuildingId = model.BuildingId.ConvertToString(),
                FloorLevel = model.FloorLevel,
                Units = model.Units,
                GridSize = model.GridSize
            };
        }
    }
}
