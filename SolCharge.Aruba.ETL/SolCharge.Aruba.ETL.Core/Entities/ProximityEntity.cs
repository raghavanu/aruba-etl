﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Enums;
using SolCharge.Aruba.ETL.Core.Extensions;
using SolCharge.Aruba.ETL.Core.Infrastructure;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class ProximityEntity : Entity
    {
        public string StaEthMac { get; set; }
        public string RadioMac { get; set; }
        public int RssiVal { get; set; }
        public string ApName { get; set; }
        public string HashedStaEthMac { get; set; }
        public TargetDevType TargetDevType { get; set; }

        public static implicit operator ProximityEntity(ProximityModel model)
        {
            if (model == null)
                return null;

            return new ProximityEntity
            {
                StaEthMac = model.StaEthMac.Addr.ConvertToMacAddress(),
                RadioMac = model.RadioMac.Addr.ConvertToMacAddress(),
                ApName = model.ApName,
                HashedStaEthMac = model.HashedStaEthMac.ConvertToMacAddress(),
                TargetDevType = EnumConverter.GetTargetDevType(model.TargetType)
            };
        }
    }
}