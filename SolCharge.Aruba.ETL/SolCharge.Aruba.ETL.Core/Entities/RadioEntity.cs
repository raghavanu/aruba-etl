﻿using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Enums;
using SolCharge.Aruba.ETL.Core.Infrastructure;
using System;
using System.Collections.Generic;

namespace SolCharge.Aruba.ETL.Core.Entities
{
    public class RadioEntity : Entity
    {
        public string ApEthMac { get; set; }
        public string RadioBssid { get; set; }
        public RadioMode Mode { get; set; }
        public PhyType Phy { get; set; }
        public HtType Ht { get; set; }
        public long RadioNum { get; set; }
        public List<VirtualAccessPointEntity> VirtualAccessPoints { get; set; }

        public static implicit operator RadioEntity(RadioModel model)
        {
            if (model == null)
                return null;

            return new RadioEntity
            {
                ApEthMac = model.ApEthMac.Addr.ToString(),
                RadioBssid = model.RadioBssid.Addr.ToString(),
                Mode = EnumConverter.GetRadioMode(model.Mode),
                Phy = EnumConverter.GetPhyType(model.Phy),
                Ht = EnumConverter.GetHTType(model.Ht),
                RadioNum = Convert.ToInt64(model.RadioNum)
            };
        }
    }
}
