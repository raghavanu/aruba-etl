﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using SolCharge.Aruba.ETL.Core.Common;
using SolCharge.Aruba.ETL.Core.Configuration;
using SolCharge.Aruba.ETL.Core.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SolCharge.Aruba.ETL.Core
{
    public class SolChargeArubaDbContext : DbContext
    {
        public SolChargeArubaDbContext(DbContextOptions<SolChargeArubaDbContext> options)
            : base(options)
        {

        }

        public DbSet<StationEntity> Station { get; set; }
        public DbSet<PresenceEntity> Presence { get; set; }
        public DbSet<ProximityEntity> Proximity { get; set; }
        public DbSet<LocationEntity> Location { get; set; }
        public DbSet<BuildingEntity> Buildings { get; set; }
        public DbSet<GeoFenceEntity> GeoFence { get; set; }
        public DbSet<FloorEntity> Floor { get; set; }
        public DbSet<GeoFenceNotifyEntity> GeoFenceNotify { get; set; }
        public DbSet<AccessPointInfoEntity> AccessPointInfo { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PresenceEntityConfiguration());
        }

        private void RunAudit()
        {
            foreach (EntityEntry entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Added))
            {
                if (entry.Entity is IDateTime entity)
                {
                    entity.CreatedAt = DateTime.Now;
                }
            }

            foreach (EntityEntry entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Deleted))
            {
                if (entry.Entity is IDateTime entity)
                {
                    entity.ModifiedAt = DateTime.Now;
                }

                entry.State = EntityState.Modified;
            }
        }

        public override int SaveChanges()
        {
            RunAudit();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            RunAudit();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            RunAudit();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
    }
}
