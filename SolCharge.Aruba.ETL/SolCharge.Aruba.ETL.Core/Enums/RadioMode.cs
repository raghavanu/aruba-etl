﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum RadioMode
    {
        RadioModeAp = 0,
        RadioModeMeshPortal = 1,
        RadioModeMeshPoint = 2,
        RadioModeAirMonitor = 3,
        RadioModeSpectrumSensor = 4,
        RadioModeUnknown = 5
    }
}
