﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum Algorithm
    {
        AlgorithmTriangulation = 0,
        AlgorithmApPlacement = 1,
        AlgorithmCalibration = 2,
        AlgorithmEstimation = 3,
        AlgorithmLowDensity = 4
    }
}
