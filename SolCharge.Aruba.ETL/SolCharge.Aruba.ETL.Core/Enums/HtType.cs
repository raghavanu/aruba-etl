﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum HtType
    {
        HttNone = 0,
        Htt20mz = 1,        
        Htt40mz = 2,
        HttVht20mz = 3,
        HttVht40mz = 4,
        HttVht80mz = 5,
        HttVht160mz = 6,
        HttVht80plus80mz = 7,
        HttInvalid = 8,
    }
}
