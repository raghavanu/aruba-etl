﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum DeploymentMode
    {
        DeploymentModeCampus = 0,
        DeploymentModeRemote = 1,
    }
}