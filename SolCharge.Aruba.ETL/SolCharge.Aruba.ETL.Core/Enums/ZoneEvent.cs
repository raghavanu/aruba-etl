﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum ZoneEvent
    {
        ZoneIn = 0,
        ZoneOut = 1,
    }
}
