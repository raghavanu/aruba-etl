﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum TrafficType
    {
        DataTrafficTypeBcast = 0,
        DataTrafficTypeMcast = 1,
        DataTrafficTypeUcast = 2,
    }
}
