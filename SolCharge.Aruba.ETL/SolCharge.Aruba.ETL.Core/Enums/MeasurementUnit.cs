﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum MeasurementUnit
    {
        Meters = 0,
        Feet = 1,
        Pixels = 2
    }
}
