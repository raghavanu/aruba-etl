﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum ErrorCode
    {
        ErrorCodeNoError = 0,
        ErrorCode0Rssi = 1,
        ErrorCodeOnly1Rssi = 2,
        ErrorCodeOnly2Rssi = 3,
        ErrorCodeRssiQuality = 4,
        ErrorCodeRssiOldTimestamp = 8,
        ErrorCodeRssiCloseTimestamp = 16,
        ErrorCodeLegacy = 1048575
    }
}
