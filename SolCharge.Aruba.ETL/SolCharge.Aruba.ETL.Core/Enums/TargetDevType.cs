﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum TargetDevType
    {
        TargetTypeUnknown = 0,
        TargetTypeStation = 1,
        TargetTypeTag = 2,
        TargetTypeUnsecure = 3
    }
}
