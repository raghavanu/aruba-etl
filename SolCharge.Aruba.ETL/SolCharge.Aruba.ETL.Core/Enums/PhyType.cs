﻿namespace SolCharge.Aruba.ETL.Core.Enums
{
    public enum PhyType
    {
        PhyType80211b = 0,
        PhyType80211a = 1,
        PhyType80211g = 2,
        PhyType80211ag = 3,
        PhyTypeInvalid = 4
    }
}
