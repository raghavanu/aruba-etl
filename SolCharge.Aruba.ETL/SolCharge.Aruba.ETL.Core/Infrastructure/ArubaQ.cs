﻿using NetMQ;
using NetMQ.Sockets;
using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Models;
using SolCharge.Aruba.ETL.Core.Constants;
using SolCharge.Aruba.ETL.Core.Extensions;
using System;
using System.Collections.Generic;
using System.IO;

namespace SolCharge.Aruba.ETL.Core.Infrastructure
{
    public class Aruba
    {
        List<TopicConstant> _topics = new List<TopicConstant>();

        private Aruba() { }

        public static Aruba Q = new Aruba();

        public Aruba Subscribe(TopicConstant topic)
        {
            if (_topics.Contains(topic))
            {
                throw new Exception($"Topic {topic} has been subscribed already.");
            }

            _topics.Add(topic);
            return this;
        }

        public Aruba Subscribe(params TopicConstant[] topics)
        {
            _topics.AddRange(topics);
            return this;
        }

        public void StartListen(string connectionString)
        {

            try
            {
                if (_topics.Count == 0)
                {
                    throw new Exception($"Minimum one topic need to be subscribed to listen");
                }

                using (var subSocket = new SubscriberSocket(connectionString))
                {
                    using (var stream = new MemoryStream())
                    {
                        subSocket.Options.ReceiveHighWatermark = 1000000;

                        foreach (var topic in _topics)
                            subSocket.Subscribe(topic.ToString());

                        while (true)
                        {
                            string topic = subSocket.ReceiveFrameString();
                            Console.WriteLine($"Topic recieved is {topic}");
                            var bytes = subSocket.ReceiveFrameBytes();
                            stream.WriteAndResetPosition(bytes, 0, bytes.Length);
                            var @event = Serializer.Deserialize<NBEventModel>(stream);

                            JobManager.Job(topic).Transform(@event);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
