﻿using SolCharge.Aruba.ETL.Core.Constants;
using SolCharge.Aruba.ETL.Core.Jobs;

namespace SolCharge.Aruba.ETL.Core.Infrastructure
{
    public static class JobManager
    {
        static IJob _presenceJob = new PresenceJob();
        static IJob _locationJob = new LocationJob();
        static IJob _buildingJob = new BuildingJob();
        static IJob _stationJob = new StationJob();
        static IJob _geoFenceNotifyJob = new GeoFenceNotifyJob();

        public static IJob Job(string topic)
        {

            if (topic == TopicConstant.Presence.ToString())
            {
                return _presenceJob;
            }

            if (topic.StartsWith(TopicConstant.Location.ToString()))
            {
                return _locationJob;
            }

            if (topic == TopicConstant.Station.ToString())
            {
                return _stationJob;
            }

            if (topic == TopicConstant.Location.ToString())
            {
                return _buildingJob;
            }

            if (topic == TopicConstant.GeoFenceNotify.ToString())
            {
                return _geoFenceNotifyJob;
            }


            return new UnknowJob();
        }
    }
}
