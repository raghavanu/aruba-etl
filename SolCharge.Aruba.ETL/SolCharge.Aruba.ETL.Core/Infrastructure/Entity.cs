﻿using SolCharge.Aruba.ETL.Core.Common;
using System;

namespace SolCharge.Aruba.ETL.Core.Infrastructure
{
    public abstract class Entity : IDateTime
    {
        public int Id { get; set; }
        public DateTimeOffset? ModifiedAt { get; set; }
        DateTimeOffset IDateTime.CreatedAt { get; set; }
    }
}
