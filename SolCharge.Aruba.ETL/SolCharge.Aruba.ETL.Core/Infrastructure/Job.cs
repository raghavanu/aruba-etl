﻿using SolCharge.Aruba.ETL.Contracts.Models;
using System;
using System.Collections.Generic;

namespace SolCharge.Aruba.ETL.Core.Infrastructure
{
    public abstract class Job : IJob
    {
        const int BatchSize = 100;

        public abstract void Transform(NBEventModel eventModel);

        public void CanWrite<TEnity>(List<TEnity> entities, object syncLock, Action callback)
            where TEnity : Entity
        {
            if (entities.Count > BatchSize)
            {
                lock (syncLock)
                {
                    if (entities.Count > BatchSize)
                    {
                        callback?.Invoke();
                        entities.Clear();
                    }
                }
            }
        }
    }
}
