﻿using SolCharge.Aruba.ETL.Contracts.Models;

namespace SolCharge.Aruba.ETL.Core.Infrastructure
{
    public interface IJob
    {
        void Transform(NBEventModel eventModel);
    }
}
