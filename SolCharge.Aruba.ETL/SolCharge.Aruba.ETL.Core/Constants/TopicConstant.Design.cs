﻿namespace SolCharge.Aruba.ETL.Core.Constants
{
    public partial class TopicConstant
    {
        private string _topic;

        private TopicConstant(string topic)
        {

            _topic = topic;
        }

        public override string ToString()
        {
            return _topic;
        }

        public override bool Equals(object obj)
        {
            return obj.ToString() == _topic;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
