﻿namespace SolCharge.Aruba.ETL.Core.Constants
{
    public partial class TopicConstant
    {
        public static TopicConstant Presence = new TopicConstant("presence");

        public static TopicConstant Building = new TopicConstant("building");

        public static TopicConstant Station = new TopicConstant("station");

        public static TopicConstant Location = new TopicConstant("location");

        public static TopicConstant GeoFence = new TopicConstant("geofence");

        public static TopicConstant GeoFenceNotify = new TopicConstant("geofence_notify");

        public static TopicConstant Controller = new TopicConstant("controller");

        public static TopicConstant Proximity = new TopicConstant("proximity");

        public static TopicConstant Floor = new TopicConstant("floor");
    }
}
