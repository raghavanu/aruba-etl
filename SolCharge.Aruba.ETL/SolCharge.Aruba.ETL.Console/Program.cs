﻿
namespace SolCharge.Aruba.ETL.Console
{
    using SolCharge.Aruba.ETL.Core.Constants;
    using SolCharge.Aruba.ETL.Core.Infrastructure;
    using System;
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Aruba ETL Version 1.0");
            Console.WriteLine("".PadRight(40, '-'));

            Aruba.Q
                .Subscribe(TopicConstant.Presence)
                .Subscribe(TopicConstant.Proximity)
                .Subscribe(TopicConstant.Building)
                .Subscribe(TopicConstant.Station)
                .Subscribe(TopicConstant.Location)
                .Subscribe(TopicConstant.GeoFence)
                .Subscribe(TopicConstant.Floor)
                .Subscribe(TopicConstant.GeoFenceNotify)
                .StartListen(">tcp://localhost:12000");

            Console.WriteLine("Press <Enter> to exit");
            Console.ReadLine();
        }
    }
}
