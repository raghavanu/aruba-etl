﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"proximity")]
    public class ProximityModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"sta_eth_mac")]
        public MacAddressModel StaEthMac { get; set; }

        [ProtoMember(2, Name = @"radio_mac")]
        public MacAddressModel RadioMac { get; set; }

        [ProtoMember(3, Name = @"rssi_val")]
        public int RssiVal
        {
            get { return __pbn__RssiVal.GetValueOrDefault(); }
            set { __pbn__RssiVal = value; }
        }
        public bool ShouldSerializeRssiVal() => __pbn__RssiVal != null;
        public void ResetRssiVal() => __pbn__RssiVal = null;
        private int? __pbn__RssiVal;

        [ProtoMember(4, Name = @"ap_name")]
        [DefaultValue("")]
        public string ApName
        {
            get { return __pbn__ApName ?? ""; }
            set { __pbn__ApName = value; }
        }
        public bool ShouldSerializeApName() => __pbn__ApName != null;
        public void ResetApName() => __pbn__ApName = null;
        private string __pbn__ApName;

        [ProtoMember(5, Name = @"hashed_sta_eth_mac")]
        public byte[] HashedStaEthMac
        {
            get { return __pbn__HashedStaEthMac; }
            set { __pbn__HashedStaEthMac = value; }
        }
        public bool ShouldSerializeHashedStaEthMac() => __pbn__HashedStaEthMac != null;
        public void ResetHashedStaEthMac() => __pbn__HashedStaEthMac = null;
        private byte[] __pbn__HashedStaEthMac;

        [ProtoMember(6, Name = @"target_type")]
        [DefaultValue(TargetDevTypeEnum.TargetTypeStation)]
        public TargetDevTypeEnum TargetType
        {
            get { return __pbn__TargetType ?? TargetDevTypeEnum.TargetTypeStation; }
            set { __pbn__TargetType = value; }
        }
        public bool ShouldSerializeTargetType() => __pbn__TargetType != null;
        public void ResetTargetType() => __pbn__TargetType = null;
        private TargetDevTypeEnum? __pbn__TargetType;

    }
}
