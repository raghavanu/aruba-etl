﻿using ProtoBuf;
using System.Collections.Generic;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"access_point")]
    public class AccessPointModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"ap_eth_mac", IsRequired = true)]
        public MacAddressModel ApEthMac { get; set; }

        [ProtoMember(2, Name = @"ap_name")]
        [DefaultValue("")]
        public string ApName
        {
            get { return __pbn__ApName ?? ""; }
            set { __pbn__ApName = value; }
        }
        public bool ShouldSerializeApName() => __pbn__ApName != null;
        public void ResetApName() => __pbn__ApName = null;
        private string __pbn__ApName;

        [ProtoMember(3, Name = @"ap_group")]
        [DefaultValue("")]
        public string ApGroup
        {
            get { return __pbn__ApGroup ?? ""; }
            set { __pbn__ApGroup = value; }
        }
        public bool ShouldSerializeApGroup() => __pbn__ApGroup != null;
        public void ResetApGroup() => __pbn__ApGroup = null;
        private string __pbn__ApGroup;

        [ProtoMember(4, Name = @"ap_model")]
        [DefaultValue("")]
        public string ApModel
        {
            get { return __pbn__ApModel ?? ""; }
            set { __pbn__ApModel = value; }
        }
        public bool ShouldSerializeApModel() => __pbn__ApModel != null;
        public void ResetApModel() => __pbn__ApModel = null;
        private string __pbn__ApModel;

        [ProtoMember(5, Name = @"depl_mode")]
        [DefaultValue(DeploymentMode.DeploymentModeCampus)]
        public DeploymentMode DeplMode
        {
            get { return __pbn__DeplMode ?? DeploymentMode.DeploymentModeCampus; }
            set { __pbn__DeplMode = value; }
        }
        public bool ShouldSerializeDeplMode() => __pbn__DeplMode != null;
        public void ResetDeplMode() => __pbn__DeplMode = null;
        private DeploymentMode? __pbn__DeplMode;

        [ProtoMember(6, Name = @"ap_ip_address")]
        public IPAddressModel ApIpAddress { get; set; }

        [ProtoMember(7, Name = @"reboots")]
        public uint Reboots
        {
            get { return __pbn__Reboots.GetValueOrDefault(); }
            set { __pbn__Reboots = value; }
        }
        public bool ShouldSerializeReboots() => __pbn__Reboots != null;
        public void ResetReboots() => __pbn__Reboots = null;
        private uint? __pbn__Reboots;

        [ProtoMember(8, Name = @"rebootstraps")]
        public uint Rebootstraps
        {
            get { return __pbn__Rebootstraps.GetValueOrDefault(); }
            set { __pbn__Rebootstraps = value; }
        }
        public bool ShouldSerializeRebootstraps() => __pbn__Rebootstraps != null;
        public void ResetRebootstraps() => __pbn__Rebootstraps = null;
        private uint? __pbn__Rebootstraps;

        [ProtoMember(9, Name = @"managed_by")]
        public IPAddressModel ManagedBy { get; set; }

        [ProtoMember(10, Name = @"managed_by_key")]
        [DefaultValue("")]
        public string ManagedByKey
        {
            get { return __pbn__ManagedByKey ?? ""; }
            set { __pbn__ManagedByKey = value; }
        }
        public bool ShouldSerializeManagedByKey() => __pbn__ManagedByKey != null;
        public void ResetManagedByKey() => __pbn__ManagedByKey = null;
        private string __pbn__ManagedByKey;

        [ProtoMember(11, Name = @"radios")]
        public List<RadioModel> Radios { get; } = new List<RadioModel>();

        [ProtoMember(12, Name = @"is_master")]
        public bool IsMaster
        {
            get { return __pbn__IsMaster.GetValueOrDefault(); }
            set { __pbn__IsMaster = value; }
        }
        public bool ShouldSerializeIsMaster() => __pbn__IsMaster != null;
        public void ResetIsMaster() => __pbn__IsMaster = null;
        private bool? __pbn__IsMaster;

        [ProtoMember(13, Name = @"reboot_reason")]
        [DefaultValue("")]
        public string RebootReason
        {
            get { return __pbn__RebootReason ?? ""; }
            set { __pbn__RebootReason = value; }
        }
        public bool ShouldSerializeRebootReason() => __pbn__RebootReason != null;
        public void ResetRebootReason() => __pbn__RebootReason = null;
        private string __pbn__RebootReason;

        [ProtoMember(14, Name = @"ap_location")]
        public AccessPointLocationModel ApLocation { get; set; }

        [ProtoContract(Name = @"deployment_mode")]
        public enum DeploymentMode
        {
            [ProtoEnum(Name = @"DEPLOYMENT_MODE_CAMPUS")]
            DeploymentModeCampus = 0,
            [ProtoEnum(Name = @"DEPLOYMENT_MODE_REMOTE")]
            DeploymentModeRemote = 1,
        }
    }
}
