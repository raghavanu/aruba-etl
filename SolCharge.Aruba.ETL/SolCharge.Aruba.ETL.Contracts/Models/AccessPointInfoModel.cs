﻿using ProtoBuf;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"Access_point_info")]
    public class AccessPointInfoModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(8, Name = @"ap_mac")]
        public MacAddressModel ApMac { get; set; }

        [ProtoMember(9, Name = @"ap_name")]
        [DefaultValue("")]
        public string ApName
        {
            get { return __pbn__ApName ?? ""; }
            set { __pbn__ApName = value; }
        }
        public bool ShouldSerializeApName() => __pbn__ApName != null;
        public void ResetApName() => __pbn__ApName = null;
        private string __pbn__ApName;

        [ProtoMember(10, Name = @"radio_bssid")]
        public MacAddressModel RadioBssid { get; set; }

        [ProtoMember(11, Name = @"rssi_val")]
        public uint RssiVal
        {
            get { return __pbn__RssiVal.GetValueOrDefault(); }
            set { __pbn__RssiVal = value; }
        }
        public bool ShouldSerializeRssiVal() => __pbn__RssiVal != null;
        public void ResetRssiVal() => __pbn__RssiVal = null;
        private uint? __pbn__RssiVal;

    }
}
