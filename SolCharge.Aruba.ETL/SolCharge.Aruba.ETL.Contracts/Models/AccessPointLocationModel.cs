﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"ap_location")]
    public class AccessPointLocationModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"ap_eth_mac", IsRequired = true)]
        public MacAddressModel ApEthMac { get; set; }

        [ProtoMember(2, Name = @"campus_id")]
        public byte[] CampusId
        {
            get { return __pbn__CampusId; }
            set { __pbn__CampusId = value; }
        }
        public bool ShouldSerializeCampusId() => __pbn__CampusId != null;
        public void ResetCampusId() => __pbn__CampusId = null;
        private byte[] __pbn__CampusId;

        [ProtoMember(3, Name = @"building_id")]
        public byte[] BuildingId
        {
            get { return __pbn__BuildingId; }
            set { __pbn__BuildingId = value; }
        }
        public bool ShouldSerializeBuildingId() => __pbn__BuildingId != null;
        public void ResetBuildingId() => __pbn__BuildingId = null;
        private byte[] __pbn__BuildingId;

        [ProtoMember(4, Name = @"floor_id")]
        public byte[] FloorId
        {
            get { return __pbn__FloorId; }
            set { __pbn__FloorId = value; }
        }
        public bool ShouldSerializeFloorId() => __pbn__FloorId != null;
        public void ResetFloorId() => __pbn__FloorId = null;
        private byte[] __pbn__FloorId;

        [ProtoMember(5, Name = @"longitude")]
        public double Longitude
        {
            get { return __pbn__Longitude.GetValueOrDefault(); }
            set { __pbn__Longitude = value; }
        }
        public bool ShouldSerializeLongitude() => __pbn__Longitude != null;
        public void ResetLongitude() => __pbn__Longitude = null;
        private double? __pbn__Longitude;

        [ProtoMember(6, Name = @"latitude")]
        public double Latitude
        {
            get { return __pbn__Latitude.GetValueOrDefault(); }
            set { __pbn__Latitude = value; }
        }
        public bool ShouldSerializeLatitude() => __pbn__Latitude != null;
        public void ResetLatitude() => __pbn__Latitude = null;
        private double? __pbn__Latitude;

        [ProtoMember(7, Name = @"ap_x")]
        public double ApX
        {
            get { return __pbn__ApX.GetValueOrDefault(); }
            set { __pbn__ApX = value; }
        }
        public bool ShouldSerializeApX() => __pbn__ApX != null;
        public void ResetApX() => __pbn__ApX = null;
        private double? __pbn__ApX;

        [ProtoMember(8, Name = @"ap_y")]
        public double ApY
        {
            get { return __pbn__ApY.GetValueOrDefault(); }
            set { __pbn__ApY = value; }
        }
        public bool ShouldSerializeApY() => __pbn__ApY != null;
        public void ResetApY() => __pbn__ApY = null;
        private double? __pbn__ApY;

    }
}
