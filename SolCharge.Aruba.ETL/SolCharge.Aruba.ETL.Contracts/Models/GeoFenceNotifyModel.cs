﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.Collections.Generic;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"geofence_notify")]
    public partial class GeoFenceNotifyModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"geofence_event")]
        [DefaultValue(ZoneEventEnum.ZoneIn)]
        public ZoneEventEnum GeofenceEvent
        {
            get { return __pbn__GeofenceEvent ?? ZoneEventEnum.ZoneIn; }
            set { __pbn__GeofenceEvent = value; }
        }
        public bool ShouldSerializeGeofenceEvent() => __pbn__GeofenceEvent != null;
        public void ResetGeofenceEvent() => __pbn__GeofenceEvent = null;
        private ZoneEventEnum? __pbn__GeofenceEvent;

        [ProtoMember(2, Name = @"geofence_id")]
        public byte[] GeofenceId
        {
            get { return __pbn__GeofenceId; }
            set { __pbn__GeofenceId = value; }
        }
        public bool ShouldSerializeGeofenceId() => __pbn__GeofenceId != null;
        public void ResetGeofenceId() => __pbn__GeofenceId = null;
        private byte[] __pbn__GeofenceId;

        [ProtoMember(3, Name = @"geofence_name")]
        [DefaultValue("")]
        public string GeofenceName
        {
            get { return __pbn__GeofenceName ?? ""; }
            set { __pbn__GeofenceName = value; }
        }
        public bool ShouldSerializeGeofenceName() => __pbn__GeofenceName != null;
        public void ResetGeofenceName() => __pbn__GeofenceName = null;
        private string __pbn__GeofenceName;

        [ProtoMember(4, Name = @"sta_mac")]
        public MacAddressModel StaMac { get; set; }

        [ProtoMember(5, Name = @"associated")]
        public bool Associated
        {
            get { return __pbn__Associated.GetValueOrDefault(); }
            set { __pbn__Associated = value; }
        }
        public bool ShouldSerializeAssociated() => __pbn__Associated != null;
        public void ResetAssociated() => __pbn__Associated = null;
        private bool? __pbn__Associated;

        [ProtoMember(6, Name = @"dwell_time")]
        [DefaultValue(0)]
        public uint DwellTime
        {
            get { return __pbn__DwellTime ?? 0; }
            set { __pbn__DwellTime = value; }
        }
        public bool ShouldSerializeDwellTime() => __pbn__DwellTime != null;
        public void ResetDwellTime() => __pbn__DwellTime = null;
        private uint? __pbn__DwellTime;

        [ProtoMember(7, Name = @"access_point_info", DataFormat = DataFormat.Group)]
        public List<AccessPointInfoModel> AccessPointInfoes { get; } = new List<AccessPointInfoModel>();

        [ProtoMember(30, Name = @"hashed_sta_mac")]
        public byte[] HashedStaMac
        {
            get { return __pbn__HashedStaMac; }
            set { __pbn__HashedStaMac = value; }
        }
        public bool ShouldSerializeHashedStaMac() => __pbn__HashedStaMac != null;
        public void ResetHashedStaMac() => __pbn__HashedStaMac = null;
        private byte[] __pbn__HashedStaMac;

        
    }
}
