﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"point")]
    public class PointModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"x")]
        public float X
        {
            get { return __pbn__X.GetValueOrDefault(); }
            set { __pbn__X = value; }
        }
        public bool ShouldSerializeX() => __pbn__X != null;
        public void ResetX() => __pbn__X = null;
        private float? __pbn__X;

        [ProtoMember(2, Name = @"y")]
        public float Y
        {
            get { return __pbn__Y.GetValueOrDefault(); }
            set { __pbn__Y = value; }
        }
        public bool ShouldSerializeY() => __pbn__Y != null;
        public void ResetY() => __pbn__Y = null;
        private float? __pbn__Y;

    }
}
