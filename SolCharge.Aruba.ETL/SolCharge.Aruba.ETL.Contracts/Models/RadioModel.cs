﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.Collections.Generic;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"radio")]
    public class RadioModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"ap_eth_mac")]
        public MacAddressModel ApEthMac { get; set; }

        [ProtoMember(2, Name = @"radio_bssid", IsRequired = true)]
        public MacAddressModel RadioBssid { get; set; }

        [ProtoMember(4, Name = @"mode")]
        [DefaultValue(RadioModeEnum.RadioModeAp)]
        public RadioModeEnum Mode
        {
            get { return __pbn__Mode ?? RadioModeEnum.RadioModeAp; }
            set { __pbn__Mode = value; }
        }
        public bool ShouldSerializeMode() => __pbn__Mode != null;
        public void ResetMode() => __pbn__Mode = null;
        private RadioModeEnum? __pbn__Mode;

        [ProtoMember(5, Name = @"phy")]
        [DefaultValue(PhyTypeEnum.PhyType80211b)]
        public PhyTypeEnum Phy
        {
            get { return __pbn__Phy ?? PhyTypeEnum.PhyType80211b; }
            set { __pbn__Phy = value; }
        }
        public bool ShouldSerializePhy() => __pbn__Phy != null;
        public void ResetPhy() => __pbn__Phy = null;
        private PhyTypeEnum? __pbn__Phy;

        [ProtoMember(6, Name = @"ht")]
        [DefaultValue(HTTypeEnum.HttNone)]
        public HTTypeEnum Ht
        {
            get { return __pbn__Ht ?? HTTypeEnum.HttNone; }
            set { __pbn__Ht = value; }
        }
        public bool ShouldSerializeHt() => __pbn__Ht != null;
        public void ResetHt() => __pbn__Ht = null;
        private HTTypeEnum? __pbn__Ht;

        [ProtoMember(7, Name = @"virtual_access_points")]
        public List<VirtualAccessPointModel> VirtualAccessPoints { get; } = new List<VirtualAccessPointModel>();

        [ProtoMember(8, Name = @"radio_num")]
        public uint RadioNum
        {
            get { return __pbn__RadioNum.GetValueOrDefault(); }
            set { __pbn__RadioNum = value; }
        }
        public bool ShouldSerializeRadioNum() => __pbn__RadioNum != null;
        public void ResetRadioNum() => __pbn__RadioNum = null;
        private uint? __pbn__RadioNum;
    }
}
