﻿using ProtoBuf;
using System.Collections.Generic;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"geofence")]
    public class GeoFenceModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"floor_id")]
        public byte[] FloorId
        {
            get { return __pbn__FloorId; }
            set { __pbn__FloorId = value; }
        }
        public bool ShouldSerializeFloorId() => __pbn__FloorId != null;
        public void ResetFloorId() => __pbn__FloorId = null;
        private byte[] __pbn__FloorId;

        [ProtoMember(2, Name = @"geofence_id")]
        public byte[] GeofenceId
        {
            get { return __pbn__GeofenceId; }
            set { __pbn__GeofenceId = value; }
        }
        public bool ShouldSerializeGeofenceId() => __pbn__GeofenceId != null;
        public void ResetGeofenceId() => __pbn__GeofenceId = null;
        private byte[] __pbn__GeofenceId;

        [ProtoMember(3, Name = @"geofence_name")]
        [DefaultValue("")]
        public string GeofenceName
        {
            get { return __pbn__GeofenceName ?? ""; }
            set { __pbn__GeofenceName = value; }
        }
        public bool ShouldSerializeGeofenceName() => __pbn__GeofenceName != null;
        public void ResetGeofenceName() => __pbn__GeofenceName = null;
        private string __pbn__GeofenceName;

        [ProtoMember(4, Name = @"type")]
        [DefaultValue("")]
        public string Type
        {
            get { return __pbn__Type ?? ""; }
            set { __pbn__Type = value; }
        }
        public bool ShouldSerializeType() => __pbn__Type != null;
        public void ResetType() => __pbn__Type = null;
        private string __pbn__Type;

        [ProtoMember(5, Name = @"point_list")]
        public List<PointModel> PointLists { get; } = new List<PointModel>();

    }
}
