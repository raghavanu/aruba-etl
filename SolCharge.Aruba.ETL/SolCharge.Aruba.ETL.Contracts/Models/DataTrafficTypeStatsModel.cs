﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"data_traffic_type_stats")]
    public partial class DataTrafficTypeStatsModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"type")]
        [DefaultValue(TrafficTypeEnum.DataTrafficTypeBcast)]
        public TrafficTypeEnum Type
        {
            get { return __pbn__Type ?? TrafficTypeEnum.DataTrafficTypeBcast; }
            set { __pbn__Type = value; }
        }
        public bool ShouldSerializeType() => __pbn__Type != null;
        public void ResetType() => __pbn__Type = null;
        private TrafficTypeEnum? __pbn__Type;

        [ProtoMember(2, Name = @"tx_frame_count")]
        public uint TxFrameCount
        {
            get { return __pbn__TxFrameCount.GetValueOrDefault(); }
            set { __pbn__TxFrameCount = value; }
        }
        public bool ShouldSerializeTxFrameCount() => __pbn__TxFrameCount != null;
        public void ResetTxFrameCount() => __pbn__TxFrameCount = null;
        private uint? __pbn__TxFrameCount;

        [ProtoMember(3, Name = @"rx_frame_count")]
        public uint RxFrameCount
        {
            get { return __pbn__RxFrameCount.GetValueOrDefault(); }
            set { __pbn__RxFrameCount = value; }
        }
        public bool ShouldSerializeRxFrameCount() => __pbn__RxFrameCount != null;
        public void ResetRxFrameCount() => __pbn__RxFrameCount = null;
        private uint? __pbn__RxFrameCount;
    }
}
