﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"data_rate_stats")]
    public class DataRateStatsModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"rate")]
        public uint Rate
        {
            get { return __pbn__Rate.GetValueOrDefault(); }
            set { __pbn__Rate = value; }
        }
        public bool ShouldSerializeRate() => __pbn__Rate != null;
        public void ResetRate() => __pbn__Rate = null;
        private uint? __pbn__Rate;

        [ProtoMember(2, Name = @"tx_frame_count")]
        public uint TxFrameCount
        {
            get { return __pbn__TxFrameCount.GetValueOrDefault(); }
            set { __pbn__TxFrameCount = value; }
        }
        public bool ShouldSerializeTxFrameCount() => __pbn__TxFrameCount != null;
        public void ResetTxFrameCount() => __pbn__TxFrameCount = null;
        private uint? __pbn__TxFrameCount;

        [ProtoMember(3, Name = @"tx_byte_count")]
        public uint TxByteCount
        {
            get { return __pbn__TxByteCount.GetValueOrDefault(); }
            set { __pbn__TxByteCount = value; }
        }
        public bool ShouldSerializeTxByteCount() => __pbn__TxByteCount != null;
        public void ResetTxByteCount() => __pbn__TxByteCount = null;
        private uint? __pbn__TxByteCount;

        [ProtoMember(4, Name = @"rx_frame_count")]
        public uint RxFrameCount
        {
            get { return __pbn__RxFrameCount.GetValueOrDefault(); }
            set { __pbn__RxFrameCount = value; }
        }
        public bool ShouldSerializeRxFrameCount() => __pbn__RxFrameCount != null;
        public void ResetRxFrameCount() => __pbn__RxFrameCount = null;
        private uint? __pbn__RxFrameCount;

        [ProtoMember(5, Name = @"rx_byte_count")]
        public uint RxByteCount
        {
            get { return __pbn__RxByteCount.GetValueOrDefault(); }
            set { __pbn__RxByteCount = value; }
        }
        public bool ShouldSerializeRxByteCount() => __pbn__RxByteCount != null;
        public void ResetRxByteCount() => __pbn__RxByteCount = null;
        private uint? __pbn__RxByteCount;
    }
}
