﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"station")]
    public class StationModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"sta_eth_mac")]
        public MacAddressModel StaEthMac { get; set; }

        [ProtoMember(2, Name = @"username")]
        [DefaultValue("")]
        public string Username
        {
            get { return __pbn__Username ?? ""; }
            set { __pbn__Username = value; }
        }
        public bool ShouldSerializeUsername() => __pbn__Username != null;
        public void ResetUsername() => __pbn__Username = null;
        private string __pbn__Username;

        [ProtoMember(3, Name = @"role")]
        [DefaultValue("")]
        public string Role
        {
            get { return __pbn__Role ?? ""; }
            set { __pbn__Role = value; }
        }
        public bool ShouldSerializeRole() => __pbn__Role != null;
        public void ResetRole() => __pbn__Role = null;
        private string __pbn__Role;

        [ProtoMember(4, Name = @"bssid")]
        public MacAddressModel Bssid { get; set; }

        [ProtoMember(5, Name = @"device_type")]
        [DefaultValue("")]
        public string DeviceType
        {
            get { return __pbn__DeviceType ?? ""; }
            set { __pbn__DeviceType = value; }
        }
        public bool ShouldSerializeDeviceType() => __pbn__DeviceType != null;
        public void ResetDeviceType() => __pbn__DeviceType = null;
        private string __pbn__DeviceType;

        [ProtoMember(6, Name = @"sta_ip_address")]
        public IPAddressModel StaIpAddress { get; set; }

        [ProtoMember(7, Name = @"hashed_sta_eth_mac")]
        public byte[] HashedStaEthMac
        {
            get { return __pbn__HashedStaEthMac; }
            set { __pbn__HashedStaEthMac = value; }
        }
        public bool ShouldSerializeHashedStaEthMac() => __pbn__HashedStaEthMac != null;
        public void ResetHashedStaEthMac() => __pbn__HashedStaEthMac = null;
        private byte[] __pbn__HashedStaEthMac;

        [ProtoMember(8, Name = @"hashed_sta_ip_address")]
        public byte[] HashedStaIpAddress
        {
            get { return __pbn__HashedStaIpAddress; }
            set { __pbn__HashedStaIpAddress = value; }
        }
        public bool ShouldSerializeHashedStaIpAddress() => __pbn__HashedStaIpAddress != null;
        public void ResetHashedStaIpAddress() => __pbn__HashedStaIpAddress = null;
        private byte[] __pbn__HashedStaIpAddress;

        [ProtoMember(9, Name = @"vlan")]
        public uint Vlan
        {
            get { return __pbn__Vlan.GetValueOrDefault(); }
            set { __pbn__Vlan = value; }
        }
        public bool ShouldSerializeVlan() => __pbn__Vlan != null;
        public void ResetVlan() => __pbn__Vlan = null;
        private uint? __pbn__Vlan;

        [ProtoMember(10, Name = @"ht")]
        [DefaultValue(HTTypeEnum.HttNone)]
        public HTTypeEnum Ht
        {
            get { return __pbn__Ht ?? HTTypeEnum.HttNone; }
            set { __pbn__Ht = value; }
        }
        public bool ShouldSerializeHt() => __pbn__Ht != null;
        public void ResetHt() => __pbn__Ht = null;
        private HTTypeEnum? __pbn__Ht;

        [ProtoMember(11, Name = @"ap_name")]
        [DefaultValue("")]
        public string ApName
        {
            get { return __pbn__ApName ?? ""; }
            set { __pbn__ApName = value; }
        }
        public bool ShouldSerializeApName() => __pbn__ApName != null;
        public void ResetApName() => __pbn__ApName = null;
        private string __pbn__ApName;

    }
}
