﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.Collections.Generic;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"stats_radio")]
    public class StatsRadioModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"ap_eth_mac")]
        public MacAddressModel ApEthMac { get; set; }

        [ProtoMember(2, Name = @"radio_number")]
        public uint RadioNumber
        {
            get { return __pbn__RadioNumber.GetValueOrDefault(); }
            set { __pbn__RadioNumber = value; }
        }
        public bool ShouldSerializeRadioNumber() => __pbn__RadioNumber != null;
        public void ResetRadioNumber() => __pbn__RadioNumber = null;
        private uint? __pbn__RadioNumber;

        [ProtoMember(3, Name = @"channel")]
        public uint Channel
        {
            get { return __pbn__Channel.GetValueOrDefault(); }
            set { __pbn__Channel = value; }
        }
        public bool ShouldSerializeChannel() => __pbn__Channel != null;
        public void ResetChannel() => __pbn__Channel = null;
        private uint? __pbn__Channel;

        [ProtoMember(4, Name = @"phy")]
        [DefaultValue(PhyTypeEnum.PhyType80211b)]
        public PhyTypeEnum Phy
        {
            get { return __pbn__Phy ?? PhyTypeEnum.PhyType80211b; }
            set { __pbn__Phy = value; }
        }
        public bool ShouldSerializePhy() => __pbn__Phy != null;
        public void ResetPhy() => __pbn__Phy = null;
        private PhyTypeEnum? __pbn__Phy;

        [ProtoMember(5, Name = @"mode")]
        [DefaultValue(RadioModeEnum.RadioModeAp)]
        public RadioModeEnum Mode
        {
            get { return __pbn__Mode ?? RadioModeEnum.RadioModeAp; }
            set { __pbn__Mode = value; }
        }
        public bool ShouldSerializeMode() => __pbn__Mode != null;
        public void ResetMode() => __pbn__Mode = null;
        private RadioModeEnum? __pbn__Mode;

        [ProtoMember(7, Name = @"noise_floor")]
        public uint NoiseFloor
        {
            get { return __pbn__NoiseFloor.GetValueOrDefault(); }
            set { __pbn__NoiseFloor = value; }
        }
        public bool ShouldSerializeNoiseFloor() => __pbn__NoiseFloor != null;
        public void ResetNoiseFloor() => __pbn__NoiseFloor = null;
        private uint? __pbn__NoiseFloor;

        [ProtoMember(8, Name = @"tx_power")]
        public uint TxPower
        {
            get { return __pbn__TxPower.GetValueOrDefault(); }
            set { __pbn__TxPower = value; }
        }
        public bool ShouldSerializeTxPower() => __pbn__TxPower != null;
        public void ResetTxPower() => __pbn__TxPower = null;
        private uint? __pbn__TxPower;

        [ProtoMember(9, Name = @"channel_utilization")]
        public uint ChannelUtilization
        {
            get { return __pbn__ChannelUtilization.GetValueOrDefault(); }
            set { __pbn__ChannelUtilization = value; }
        }
        public bool ShouldSerializeChannelUtilization() => __pbn__ChannelUtilization != null;
        public void ResetChannelUtilization() => __pbn__ChannelUtilization = null;
        private uint? __pbn__ChannelUtilization;

        [ProtoMember(10, Name = @"rx_channel_utilization")]
        public uint RxChannelUtilization
        {
            get { return __pbn__RxChannelUtilization.GetValueOrDefault(); }
            set { __pbn__RxChannelUtilization = value; }
        }
        public bool ShouldSerializeRxChannelUtilization() => __pbn__RxChannelUtilization != null;
        public void ResetRxChannelUtilization() => __pbn__RxChannelUtilization = null;
        private uint? __pbn__RxChannelUtilization;

        [ProtoMember(11, Name = @"tx_channel_utilization")]
        public uint TxChannelUtilization
        {
            get { return __pbn__TxChannelUtilization.GetValueOrDefault(); }
            set { __pbn__TxChannelUtilization = value; }
        }
        public bool ShouldSerializeTxChannelUtilization() => __pbn__TxChannelUtilization != null;
        public void ResetTxChannelUtilization() => __pbn__TxChannelUtilization = null;
        private uint? __pbn__TxChannelUtilization;

        [ProtoMember(12, Name = @"tx_received")]
        public uint TxReceived
        {
            get { return __pbn__TxReceived.GetValueOrDefault(); }
            set { __pbn__TxReceived = value; }
        }
        public bool ShouldSerializeTxReceived() => __pbn__TxReceived != null;
        public void ResetTxReceived() => __pbn__TxReceived = null;
        private uint? __pbn__TxReceived;

        [ProtoMember(13, Name = @"tx_transmitted")]
        public uint TxTransmitted
        {
            get { return __pbn__TxTransmitted.GetValueOrDefault(); }
            set { __pbn__TxTransmitted = value; }
        }
        public bool ShouldSerializeTxTransmitted() => __pbn__TxTransmitted != null;
        public void ResetTxTransmitted() => __pbn__TxTransmitted = null;
        private uint? __pbn__TxTransmitted;

        [ProtoMember(14, Name = @"tx_dropped")]
        public uint TxDropped
        {
            get { return __pbn__TxDropped.GetValueOrDefault(); }
            set { __pbn__TxDropped = value; }
        }
        public bool ShouldSerializeTxDropped() => __pbn__TxDropped != null;
        public void ResetTxDropped() => __pbn__TxDropped = null;
        private uint? __pbn__TxDropped;

        [ProtoMember(15, Name = @"tx_data_received")]
        public uint TxDataReceived
        {
            get { return __pbn__TxDataReceived.GetValueOrDefault(); }
            set { __pbn__TxDataReceived = value; }
        }
        public bool ShouldSerializeTxDataReceived() => __pbn__TxDataReceived != null;
        public void ResetTxDataReceived() => __pbn__TxDataReceived = null;
        private uint? __pbn__TxDataReceived;

        [ProtoMember(16, Name = @"tx_data_transmitted")]
        public uint TxDataTransmitted
        {
            get { return __pbn__TxDataTransmitted.GetValueOrDefault(); }
            set { __pbn__TxDataTransmitted = value; }
        }
        public bool ShouldSerializeTxDataTransmitted() => __pbn__TxDataTransmitted != null;
        public void ResetTxDataTransmitted() => __pbn__TxDataTransmitted = null;
        private uint? __pbn__TxDataTransmitted;

        [ProtoMember(17, Name = @"tx_data_retried")]
        public uint TxDataRetried
        {
            get { return __pbn__TxDataRetried.GetValueOrDefault(); }
            set { __pbn__TxDataRetried = value; }
        }
        public bool ShouldSerializeTxDataRetried() => __pbn__TxDataRetried != null;
        public void ResetTxDataRetried() => __pbn__TxDataRetried = null;
        private uint? __pbn__TxDataRetried;

        [ProtoMember(18, Name = @"rx_frames")]
        public uint RxFrames
        {
            get { return __pbn__RxFrames.GetValueOrDefault(); }
            set { __pbn__RxFrames = value; }
        }
        public bool ShouldSerializeRxFrames() => __pbn__RxFrames != null;
        public void ResetRxFrames() => __pbn__RxFrames = null;
        private uint? __pbn__RxFrames;

        [ProtoMember(19, Name = @"rx_retried")]
        public uint RxRetried
        {
            get { return __pbn__RxRetried.GetValueOrDefault(); }
            set { __pbn__RxRetried = value; }
        }
        public bool ShouldSerializeRxRetried() => __pbn__RxRetried != null;
        public void ResetRxRetried() => __pbn__RxRetried = null;
        private uint? __pbn__RxRetried;

        [ProtoMember(20, Name = @"rx_data_frames")]
        public uint RxDataFrames
        {
            get { return __pbn__RxDataFrames.GetValueOrDefault(); }
            set { __pbn__RxDataFrames = value; }
        }
        public bool ShouldSerializeRxDataFrames() => __pbn__RxDataFrames != null;
        public void ResetRxDataFrames() => __pbn__RxDataFrames = null;
        private uint? __pbn__RxDataFrames;

        [ProtoMember(21, Name = @"rx_data_retried")]
        public uint RxDataRetried
        {
            get { return __pbn__RxDataRetried.GetValueOrDefault(); }
            set { __pbn__RxDataRetried = value; }
        }
        public bool ShouldSerializeRxDataRetried() => __pbn__RxDataRetried != null;
        public void ResetRxDataRetried() => __pbn__RxDataRetried = null;
        private uint? __pbn__RxDataRetried;

        [ProtoMember(22, Name = @"rx_frame_errors")]
        public uint RxFrameErrors
        {
            get { return __pbn__RxFrameErrors.GetValueOrDefault(); }
            set { __pbn__RxFrameErrors = value; }
        }
        public bool ShouldSerializeRxFrameErrors() => __pbn__RxFrameErrors != null;
        public void ResetRxFrameErrors() => __pbn__RxFrameErrors = null;
        private uint? __pbn__RxFrameErrors;

        [ProtoMember(23, Name = @"traffic_stats")]
        public List<DataTrafficTypeStatsModel> TrafficStats { get; } = new List<DataTrafficTypeStatsModel>();

        [ProtoMember(24, Name = @"prio_stats")]
        public List<DataPrioStatsModel> PrioStats { get; } = new List<DataPrioStatsModel>();

        [ProtoMember(25, Name = @"rate_stats")]
        public List<DataRateStatsModel> RateStats { get; } = new List<DataRateStatsModel>();

        [ProtoMember(26, Name = @"actual_eirp")]
        public uint ActualEirp
        {
            get { return __pbn__ActualEirp.GetValueOrDefault(); }
            set { __pbn__ActualEirp = value; }
        }
        public bool ShouldSerializeActualEirp() => __pbn__ActualEirp != null;
        public void ResetActualEirp() => __pbn__ActualEirp = null;
        private uint? __pbn__ActualEirp;

        [ProtoMember(27, Name = @"radio_mac")]
        public MacAddressModel RadioMac { get; set; }

        [ProtoMember(28, Name = @"tx_data_bytes")]
        public ulong TxDataBytes
        {
            get { return __pbn__TxDataBytes.GetValueOrDefault(); }
            set { __pbn__TxDataBytes = value; }
        }
        public bool ShouldSerializeTxDataBytes() => __pbn__TxDataBytes != null;
        public void ResetTxDataBytes() => __pbn__TxDataBytes = null;
        private ulong? __pbn__TxDataBytes;

        [ProtoMember(29, Name = @"rx_data_bytes")]
        public ulong RxDataBytes
        {
            get { return __pbn__RxDataBytes.GetValueOrDefault(); }
            set { __pbn__RxDataBytes = value; }
        }
        public bool ShouldSerializeRxDataBytes() => __pbn__RxDataBytes != null;
        public void ResetRxDataBytes() => __pbn__RxDataBytes = null;
        private ulong? __pbn__RxDataBytes;

        [ProtoMember(30, Name = @"radio_band")]
        public uint RadioBand
        {
            get { return __pbn__RadioBand.GetValueOrDefault(); }
            set { __pbn__RadioBand = value; }
        }
        public bool ShouldSerializeRadioBand() => __pbn__RadioBand != null;
        public void ResetRadioBand() => __pbn__RadioBand = null;
        private uint? __pbn__RadioBand;

        [ProtoMember(31, Name = @"channel_busy_64")]
        public uint ChannelBusy64
        {
            get { return __pbn__ChannelBusy64.GetValueOrDefault(); }
            set { __pbn__ChannelBusy64 = value; }
        }
        public bool ShouldSerializeChannelBusy64() => __pbn__ChannelBusy64 != null;
        public void ResetChannelBusy64() => __pbn__ChannelBusy64 = null;
        private uint? __pbn__ChannelBusy64;

        [ProtoMember(32, Name = @"sta_number")]
        public uint StaNumber
        {
            get { return __pbn__StaNumber.GetValueOrDefault(); }
            set { __pbn__StaNumber = value; }
        }
        public bool ShouldSerializeStaNumber() => __pbn__StaNumber != null;
        public void ResetStaNumber() => __pbn__StaNumber = null;
        private uint? __pbn__StaNumber;
    }
}
