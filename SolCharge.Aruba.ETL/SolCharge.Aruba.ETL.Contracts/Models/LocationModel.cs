﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.Collections.Generic;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"location")]
    public class LocationModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"sta_eth_mac")]
        public MacAddressModel StaEthMac { get; set; }

        [ProtoMember(2, Name = @"sta_location_x")]
        public float StaLocationX
        {
            get { return __pbn__StaLocationX.GetValueOrDefault(); }
            set { __pbn__StaLocationX = value; }
        }
        public bool ShouldSerializeStaLocationX() => __pbn__StaLocationX != null;
        public void ResetStaLocationX() => __pbn__StaLocationX = null;
        private float? __pbn__StaLocationX;

        [ProtoMember(3, Name = @"sta_location_y")]
        public float StaLocationY
        {
            get { return __pbn__StaLocationY.GetValueOrDefault(); }
            set { __pbn__StaLocationY = value; }
        }
        public bool ShouldSerializeStaLocationY() => __pbn__StaLocationY != null;
        public void ResetStaLocationY() => __pbn__StaLocationY = null;
        private float? __pbn__StaLocationY;

        [ProtoMember(7, Name = @"error_level")]
        public uint ErrorLevel
        {
            get { return __pbn__ErrorLevel.GetValueOrDefault(); }
            set { __pbn__ErrorLevel = value; }
        }
        public bool ShouldSerializeErrorLevel() => __pbn__ErrorLevel != null;
        public void ResetErrorLevel() => __pbn__ErrorLevel = null;
        private uint? __pbn__ErrorLevel;

        [ProtoMember(8, Name = @"associated")]
        public bool Associated
        {
            get { return __pbn__Associated.GetValueOrDefault(); }
            set { __pbn__Associated = value; }
        }
        public bool ShouldSerializeAssociated() => __pbn__Associated != null;
        public void ResetAssociated() => __pbn__Associated = null;
        private bool? __pbn__Associated;

        [ProtoMember(9, Name = @"campus_id")]
        public byte[] CampusId
        {
            get { return __pbn__CampusId; }
            set { __pbn__CampusId = value; }
        }
        public bool ShouldSerializeCampusId() => __pbn__CampusId != null;
        public void ResetCampusId() => __pbn__CampusId = null;
        private byte[] __pbn__CampusId;

        [ProtoMember(10, Name = @"building_id")]
        public byte[] BuildingId
        {
            get { return __pbn__BuildingId; }
            set { __pbn__BuildingId = value; }
        }
        public bool ShouldSerializeBuildingId() => __pbn__BuildingId != null;
        public void ResetBuildingId() => __pbn__BuildingId = null;
        private byte[] __pbn__BuildingId;

        [ProtoMember(11, Name = @"floor_id")]
        public byte[] FloorId
        {
            get { return __pbn__FloorId; }
            set { __pbn__FloorId = value; }
        }
        public bool ShouldSerializeFloorId() => __pbn__FloorId != null;
        public void ResetFloorId() => __pbn__FloorId = null;
        private byte[] __pbn__FloorId;

        [ProtoMember(12, Name = @"hashed_sta_eth_mac")]
        public byte[] HashedStaEthMac
        {
            get { return __pbn__HashedStaEthMac; }
            set { __pbn__HashedStaEthMac = value; }
        }
        public bool ShouldSerializeHashedStaEthMac() => __pbn__HashedStaEthMac != null;
        public void ResetHashedStaEthMac() => __pbn__HashedStaEthMac = null;
        private byte[] __pbn__HashedStaEthMac;

        [ProtoMember(13, Name = @"geofence_ids")]
        public List<byte[]> GeofenceIds { get; } = new List<byte[]>();

        [ProtoMember(14, Name = @"loc_algorithm")]
        [DefaultValue(AlgorithmEnum.AlgorithmTriangulation)]
        public AlgorithmEnum LocAlgorithm
        {
            get { return __pbn__LocAlgorithm ?? AlgorithmEnum.AlgorithmTriangulation; }
            set { __pbn__LocAlgorithm = value; }
        }
        public bool ShouldSerializeLocAlgorithm() => __pbn__LocAlgorithm != null;
        public void ResetLocAlgorithm() => __pbn__LocAlgorithm = null;
        private AlgorithmEnum? __pbn__LocAlgorithm;

        [ProtoMember(15, Name = @"rssi_val")]
        public uint RssiVal
        {
            get { return __pbn__RssiVal.GetValueOrDefault(); }
            set { __pbn__RssiVal = value; }
        }
        public bool ShouldSerializeRssiVal() => __pbn__RssiVal != null;
        public void ResetRssiVal() => __pbn__RssiVal = null;
        private uint? __pbn__RssiVal;

        [ProtoMember(16, Name = @"longitude")]
        public double Longitude
        {
            get { return __pbn__Longitude.GetValueOrDefault(); }
            set { __pbn__Longitude = value; }
        }
        public bool ShouldSerializeLongitude() => __pbn__Longitude != null;
        public void ResetLongitude() => __pbn__Longitude = null;
        private double? __pbn__Longitude;

        [ProtoMember(17, Name = @"latitude")]
        public double Latitude
        {
            get { return __pbn__Latitude.GetValueOrDefault(); }
            set { __pbn__Latitude = value; }
        }
        public bool ShouldSerializeLatitude() => __pbn__Latitude != null;
        public void ResetLatitude() => __pbn__Latitude = null;
        private double? __pbn__Latitude;

        [ProtoMember(18, Name = @"altitude")]
        public double Altitude
        {
            get { return __pbn__Altitude.GetValueOrDefault(); }
            set { __pbn__Altitude = value; }
        }
        public bool ShouldSerializeAltitude() => __pbn__Altitude != null;
        public void ResetAltitude() => __pbn__Altitude = null;
        private double? __pbn__Altitude;

        [ProtoMember(19, Name = @"unit")]
        [DefaultValue(MeasurementUnitEnum.Meters)]
        public MeasurementUnitEnum Unit
        {
            get { return __pbn__Unit ?? MeasurementUnitEnum.Meters; }
            set { __pbn__Unit = value; }
        }
        public bool ShouldSerializeUnit() => __pbn__Unit != null;
        public void ResetUnit() => __pbn__Unit = null;
        private MeasurementUnitEnum? __pbn__Unit;

        [ProtoMember(20, Name = @"target_type")]
        [DefaultValue(TargetDevTypeEnum.TargetTypeStation)]
        public TargetDevTypeEnum TargetType
        {
            get { return __pbn__TargetType ?? TargetDevTypeEnum.TargetTypeStation; }
            set { __pbn__TargetType = value; }
        }
        public bool ShouldSerializeTargetType() => __pbn__TargetType != null;
        public void ResetTargetType() => __pbn__TargetType = null;
        private TargetDevTypeEnum? __pbn__TargetType;

        [ProtoMember(21, Name = @"err_code")]
        [DefaultValue(ErrorCodeEnum.ErrorCodeNoError)]
        public ErrorCodeEnum ErrCode
        {
            get { return __pbn__ErrCode ?? ErrorCodeEnum.ErrorCodeNoError; }
            set { __pbn__ErrCode = value; }
        }
        public bool ShouldSerializeErrCode() => __pbn__ErrCode != null;
        public void ResetErrCode() => __pbn__ErrCode = null;
        private ErrorCodeEnum? __pbn__ErrCode;

        [ProtoMember(22, Name = @"records")]
        public List<Record> Records { get; } = new List<Record>();

        [ProtoContract(Name = @"record")]
        public partial class Record : IExtensible
        {
            private IExtension __pbn__extensionData;
            IExtension IExtensible.GetExtensionObject(bool createIfMissing)
                => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

            [ProtoMember(1, Name = @"timestamp", IsRequired = true)]
            public uint Timestamp { get; set; }

            [ProtoMember(2, Name = @"radio_mac", IsRequired = true)]
            public MacAddressModel RadioMac { get; set; }

            [ProtoMember(3, Name = @"rssi_val", IsRequired = true)]
            public int RssiVal { get; set; }

            [ProtoMember(4, Name = @"channel")]
            public uint Channel
            {
                get { return __pbn__Channel.GetValueOrDefault(); }
                set { __pbn__Channel = value; }
            }
            public bool ShouldSerializeChannel() => __pbn__Channel != null;
            public void ResetChannel() => __pbn__Channel = null;
            private uint? __pbn__Channel;
        }
    }
}
