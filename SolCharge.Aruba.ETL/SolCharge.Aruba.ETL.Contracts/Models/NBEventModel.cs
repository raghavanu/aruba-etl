﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"nb_event")]
    public partial class NBEventModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"seq")]
        public ulong Seq
        {
            get { return __pbn__Seq.GetValueOrDefault(); }
            set { __pbn__Seq = value; }
        }
        public bool ShouldSerializeSeq() => __pbn__Seq != null;
        public void ResetSeq() => __pbn__Seq = null;
        private ulong? __pbn__Seq;

        [ProtoMember(2, Name = @"timestamp")]
        public uint Timestamp
        {
            get { return __pbn__Timestamp.GetValueOrDefault(); }
            set { __pbn__Timestamp = value; }
        }
        public bool ShouldSerializeTimestamp() => __pbn__Timestamp != null;
        public void ResetTimestamp() => __pbn__Timestamp = null;
        private uint? __pbn__Timestamp;

        [ProtoMember(3, Name = @"op")]
        [DefaultValue(EventOperationEnum.OpAdd)]
        public EventOperationEnum Op
        {
            get { return __pbn__Op ?? EventOperationEnum.OpAdd; }
            set { __pbn__Op = value; }
        }
        public bool ShouldSerializeOp() => __pbn__Op != null;
        public void ResetOp() => __pbn__Op = null;
        private EventOperationEnum? __pbn__Op;

        [ProtoMember(4, Name = @"topic_seq")]
        public ulong TopicSeq
        {
            get { return __pbn__TopicSeq.GetValueOrDefault(); }
            set { __pbn__TopicSeq = value; }
        }
        public bool ShouldSerializeTopicSeq() => __pbn__TopicSeq != null;
        public void ResetTopicSeq() => __pbn__TopicSeq = null;
        private ulong? __pbn__TopicSeq;

        [ProtoMember(5, Name = @"source_id")]
        public byte[] SourceId
        {
            get { return __pbn__SourceId; }
            set { __pbn__SourceId = value; }
        }
        public bool ShouldSerializeSourceId() => __pbn__SourceId != null;
        public void ResetSourceId() => __pbn__SourceId = null;
        private byte[] __pbn__SourceId;

        [ProtoMember(6, Name = @"lic_info")]
        [DefaultValue(LicenseInfoEnum.hbDhak)]
        public LicenseInfoEnum LicInfo
        {
            get { return __pbn__LicInfo ?? LicenseInfoEnum.hbDhak; }
            set { __pbn__LicInfo = value; }
        }
        public bool ShouldSerializeLicInfo() => __pbn__LicInfo != null;
        public void ResetLicInfo() => __pbn__LicInfo = null;
        private LicenseInfoEnum? __pbn__LicInfo;

        [ProtoMember(500, Name = @"location")]
        public LocationModel Location { get; set; }

        [ProtoMember(501, Name = @"presence")]
        public PresenceModel Presence { get; set; }

        [ProtoMember(503, Name = @"station")]
        public StationModel Station { get; set; }

        [ProtoMember(512, Name = @"building")]
        public BuildingModel Building { get; set; }

        [ProtoMember(517, Name = @"geofence_notify")]
        public GeoFenceNotifyModel GeofenceNotify { get; set; }
    }
}
