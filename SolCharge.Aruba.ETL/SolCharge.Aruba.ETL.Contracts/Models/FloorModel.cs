﻿using ProtoBuf;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"floor")]
    public class FloorModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"floor_id")]
        public byte[] FloorId
        {
            get { return __pbn__FloorId; }
            set { __pbn__FloorId = value; }
        }
        public bool ShouldSerializeFloorId() => __pbn__FloorId != null;
        public void ResetFloorId() => __pbn__FloorId = null;
        private byte[] __pbn__FloorId;

        [ProtoMember(2, Name = @"floor_name")]
        [DefaultValue("")]
        public string FloorName
        {
            get { return __pbn__FloorName ?? ""; }
            set { __pbn__FloorName = value; }
        }
        public bool ShouldSerializeFloorName() => __pbn__FloorName != null;
        public void ResetFloorName() => __pbn__FloorName = null;
        private string __pbn__FloorName;

        [ProtoMember(3, Name = @"floor_latitude")]
        public float FloorLatitude
        {
            get { return __pbn__FloorLatitude.GetValueOrDefault(); }
            set { __pbn__FloorLatitude = value; }
        }
        public bool ShouldSerializeFloorLatitude() => __pbn__FloorLatitude != null;
        public void ResetFloorLatitude() => __pbn__FloorLatitude = null;
        private float? __pbn__FloorLatitude;

        [ProtoMember(4, Name = @"floor_longitude")]
        public float FloorLongitude
        {
            get { return __pbn__FloorLongitude.GetValueOrDefault(); }
            set { __pbn__FloorLongitude = value; }
        }
        public bool ShouldSerializeFloorLongitude() => __pbn__FloorLongitude != null;
        public void ResetFloorLongitude() => __pbn__FloorLongitude = null;
        private float? __pbn__FloorLongitude;

        [ProtoMember(5, Name = @"floor_img_path")]
        [DefaultValue("")]
        public string FloorImgPath
        {
            get { return __pbn__FloorImgPath ?? ""; }
            set { __pbn__FloorImgPath = value; }
        }
        public bool ShouldSerializeFloorImgPath() => __pbn__FloorImgPath != null;
        public void ResetFloorImgPath() => __pbn__FloorImgPath = null;
        private string __pbn__FloorImgPath;

        [ProtoMember(6, Name = @"floor_img_width")]
        public float FloorImgWidth
        {
            get { return __pbn__FloorImgWidth.GetValueOrDefault(); }
            set { __pbn__FloorImgWidth = value; }
        }
        public bool ShouldSerializeFloorImgWidth() => __pbn__FloorImgWidth != null;
        public void ResetFloorImgWidth() => __pbn__FloorImgWidth = null;
        private float? __pbn__FloorImgWidth;

        [ProtoMember(7, Name = @"floor_img_length")]
        public float FloorImgLength
        {
            get { return __pbn__FloorImgLength.GetValueOrDefault(); }
            set { __pbn__FloorImgLength = value; }
        }
        public bool ShouldSerializeFloorImgLength() => __pbn__FloorImgLength != null;
        public void ResetFloorImgLength() => __pbn__FloorImgLength = null;
        private float? __pbn__FloorImgLength;

        [ProtoMember(8, Name = @"building_id")]
        public byte[] BuildingId
        {
            get { return __pbn__BuildingId; }
            set { __pbn__BuildingId = value; }
        }
        public bool ShouldSerializeBuildingId() => __pbn__BuildingId != null;
        public void ResetBuildingId() => __pbn__BuildingId = null;
        private byte[] __pbn__BuildingId;

        [ProtoMember(9, Name = @"floor_level")]
        public float FloorLevel
        {
            get { return __pbn__FloorLevel.GetValueOrDefault(); }
            set { __pbn__FloorLevel = value; }
        }
        public bool ShouldSerializeFloorLevel() => __pbn__FloorLevel != null;
        public void ResetFloorLevel() => __pbn__FloorLevel = null;
        private float? __pbn__FloorLevel;

        [ProtoMember(10, Name = @"units")]
        [DefaultValue("")]
        public string Units
        {
            get { return __pbn__Units ?? ""; }
            set { __pbn__Units = value; }
        }
        public bool ShouldSerializeUnits() => __pbn__Units != null;
        public void ResetUnits() => __pbn__Units = null;
        private string __pbn__Units;

        [ProtoMember(11, Name = @"grid_size")]
        public float GridSize
        {
            get { return __pbn__GridSize.GetValueOrDefault(); }
            set { __pbn__GridSize = value; }
        }
        public bool ShouldSerializeGridSize() => __pbn__GridSize != null;
        public void ResetGridSize() => __pbn__GridSize = null;
        private float? __pbn__GridSize;

    }
}
