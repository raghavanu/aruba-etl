﻿using ProtoBuf;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"building")]
    public class BuildingModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"building_id")]
        public byte[] BuildingId
        {
            get { return __pbn__BuildingId; }
            set { __pbn__BuildingId = value; }
        }
        public bool ShouldSerializeBuildingId() => __pbn__BuildingId != null;
        public void ResetBuildingId() => __pbn__BuildingId = null;
        private byte[] __pbn__BuildingId;

        [ProtoMember(2, Name = @"building_name")]
        [DefaultValue("")]
        public string BuildingName
        {
            get { return __pbn__BuildingName ?? ""; }
            set { __pbn__BuildingName = value; }
        }
        public bool ShouldSerializeBuildingName() => __pbn__BuildingName != null;
        public void ResetBuildingName() => __pbn__BuildingName = null;
        private string __pbn__BuildingName;

        [ProtoMember(3, Name = @"campus_id")]
        public byte[] CampusId
        {
            get { return __pbn__CampusId; }
            set { __pbn__CampusId = value; }
        }
        public bool ShouldSerializeCampusId() => __pbn__CampusId != null;
        public void ResetCampusId() => __pbn__CampusId = null;
        private byte[] __pbn__CampusId;

    }
}
