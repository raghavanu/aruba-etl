﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"mac_address")]
    public class MacAddressModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"addr", IsRequired = true)]
        public byte[] Addr { get; set; }
    }
}
