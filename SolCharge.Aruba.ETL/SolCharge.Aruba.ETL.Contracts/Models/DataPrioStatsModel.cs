﻿using ProtoBuf;
using SolCharge.Aruba.ETL.Contracts.Enums;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"data_prio_stats")]
    public class DataPrioStatsModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"prio")]
        [DefaultValue(DataPrioEnum.DataPrioBk)]
        public DataPrioEnum Prio
        {
            get { return __pbn__Prio ?? DataPrioEnum.DataPrioBk; }
            set { __pbn__Prio = value; }
        }
        public bool ShouldSerializePrio() => __pbn__Prio != null;
        public void ResetPrio() => __pbn__Prio = null;
        private DataPrioEnum? __pbn__Prio;

        [ProtoMember(2, Name = @"tx_frame_count")]
        public uint TxFrameCount
        {
            get { return __pbn__TxFrameCount.GetValueOrDefault(); }
            set { __pbn__TxFrameCount = value; }
        }
        public bool ShouldSerializeTxFrameCount() => __pbn__TxFrameCount != null;
        public void ResetTxFrameCount() => __pbn__TxFrameCount = null;
        private uint? __pbn__TxFrameCount;

        [ProtoMember(3, Name = @"rx_frame_count")]
        public uint RxFrameCount
        {
            get { return __pbn__RxFrameCount.GetValueOrDefault(); }
            set { __pbn__RxFrameCount = value; }
        }
        public bool ShouldSerializeRxFrameCount() => __pbn__RxFrameCount != null;
        public void ResetRxFrameCount() => __pbn__RxFrameCount = null;
        private uint? __pbn__RxFrameCount;

        [ProtoMember(4, Name = @"tx_drop_count")]
        public uint TxDropCount
        {
            get { return __pbn__TxDropCount.GetValueOrDefault(); }
            set { __pbn__TxDropCount = value; }
        }
        public bool ShouldSerializeTxDropCount() => __pbn__TxDropCount != null;
        public void ResetTxDropCount() => __pbn__TxDropCount = null;
        private uint? __pbn__TxDropCount;

    }
}
