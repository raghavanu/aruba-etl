﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"ip_address")]
    public class IPAddressModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"af", IsRequired = true)]
        public AddrFamily Af { get; set; }

        [ProtoMember(2, Name = @"addr", IsRequired = true)]
        public byte[] Addr { get; set; }

        [ProtoContract(Name = @"addr_family")]
        public enum AddrFamily
        {
            [ProtoEnum(Name = @"ADDR_FAMILY_UNSPEC")]
            AddrFamilyUnspec = 0,
            [ProtoEnum(Name = @"ADDR_FAMILY_INET")]
            AddrFamilyInet = 2,
            [ProtoEnum(Name = @"ADDR_FAMILY_INET6")]
            AddrFamilyInet6 = 10,
        }
    }
}
