﻿using ProtoBuf;
using System.ComponentModel;

namespace SolCharge.Aruba.ETL.Contracts.Models
{
    [ProtoContract(Name = @"virtual_access_point")]
    public class VirtualAccessPointModel : IExtensible
    {
        private IExtension __pbn__extensionData;
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            => Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [ProtoMember(1, Name = @"bssid", IsRequired = true)]
        public MacAddressModel Bssid { get; set; }

        [ProtoMember(2, Name = @"ssid")]
        [DefaultValue("")]
        public string Ssid
        {
            get { return __pbn__Ssid ?? ""; }
            set { __pbn__Ssid = value; }
        }
        public bool ShouldSerializeSsid() => __pbn__Ssid != null;
        public void ResetSsid() => __pbn__Ssid = null;
        private string __pbn__Ssid;

        [ProtoMember(3, Name = @"radio_bssid")]
        public MacAddressModel RadioBssid { get; set; }
    }
}
