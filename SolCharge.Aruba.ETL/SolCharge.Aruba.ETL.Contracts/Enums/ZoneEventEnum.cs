﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"zone_event")]
    public enum ZoneEventEnum
    {
        [ProtoEnum(Name = @"ZONE_IN")]
        ZoneIn = 0,
        [ProtoEnum(Name = @"ZONE_OUT")]
        ZoneOut = 1,
    }
}
