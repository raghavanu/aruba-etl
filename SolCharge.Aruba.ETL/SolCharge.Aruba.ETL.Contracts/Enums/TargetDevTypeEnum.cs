﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"target_dev_type")]
    public enum TargetDevTypeEnum
    {
        [ProtoEnum(Name = @"TARGET_TYPE_UNKNOWN")]
        TargetTypeUnknown = 0,
        [ProtoEnum(Name = @"TARGET_TYPE_STATION")]
        TargetTypeStation = 1,
        [ProtoEnum(Name = @"TARGET_TYPE_TAG")]
        TargetTypeTag = 2,
        [ProtoEnum(Name = @"TARGET_TYPE_UNSECURE")]
        TargetTypeUnsecure = 3,
    }
}
