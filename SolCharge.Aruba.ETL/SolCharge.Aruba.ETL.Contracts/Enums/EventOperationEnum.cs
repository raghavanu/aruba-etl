﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"event_operation")]
    public enum EventOperationEnum
    {
        [ProtoEnum(Name = @"OP_ADD")]
        OpAdd = 0,
        [ProtoEnum(Name = @"OP_UPDATE")]
        OpUpdate = 1,
        [ProtoEnum(Name = @"OP_DELETE")]
        OpDelete = 2,
        [ProtoEnum(Name = @"OP_SYNC")]
        OpSync = 3,
    }
}
