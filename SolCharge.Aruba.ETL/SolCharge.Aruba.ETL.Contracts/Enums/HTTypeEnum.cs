﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"ht_type")]
    public enum HTTypeEnum
    {
        [ProtoEnum(Name = @"HTT_NONE")]
        HttNone = 0,
        [ProtoEnum(Name = @"HTT_20MZ")]
        Htt20mz = 1,
        [ProtoEnum(Name = @"HTT_40MZ")]
        Htt40mz = 2,
        [ProtoEnum(Name = @"HTT_VHT_20MZ")]
        HttVht20mz = 3,
        [ProtoEnum(Name = @"HTT_VHT_40MZ")]
        HttVht40mz = 4,
        [ProtoEnum(Name = @"HTT_VHT_80MZ")]
        HttVht80mz = 5,
        [ProtoEnum(Name = @"HTT_VHT_160MZ")]
        HttVht160mz = 6,
        [ProtoEnum(Name = @"HTT_VHT_80PLUS80MZ")]
        HttVht80plus80mz = 7,
        [ProtoEnum(Name = @"HTT_INVALID")]
        HttInvalid = 8,
    }
}
