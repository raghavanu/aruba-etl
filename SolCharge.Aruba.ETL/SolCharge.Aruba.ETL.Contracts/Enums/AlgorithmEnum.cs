﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"algorithm")]
    public enum AlgorithmEnum
    {
        [ProtoEnum(Name = @"ALGORITHM_TRIANGULATION")]
        AlgorithmTriangulation = 0,
        [ProtoEnum(Name = @"ALGORITHM_AP_PLACEMENT")]
        AlgorithmApPlacement = 1,
        [ProtoEnum(Name = @"ALGORITHM_CALIBRATION")]
        AlgorithmCalibration = 2,
        [ProtoEnum(Name = @"ALGORITHM_ESTIMATION")]
        AlgorithmEstimation = 3,
        [ProtoEnum(Name = @"ALGORITHM_LOW_DENSITY")]
        AlgorithmLowDensity = 4,
    }
}
