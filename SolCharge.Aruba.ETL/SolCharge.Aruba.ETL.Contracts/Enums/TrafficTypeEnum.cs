﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"traffic_type")]
    public enum TrafficTypeEnum
    {
        [ProtoEnum(Name = @"DATA_TRAFFIC_TYPE_BCAST")]
        DataTrafficTypeBcast = 0,
        [ProtoEnum(Name = @"DATA_TRAFFIC_TYPE_MCAST")]
        DataTrafficTypeMcast = 1,
        [ProtoEnum(Name = @"DATA_TRAFFIC_TYPE_UCAST")]
        DataTrafficTypeUcast = 2,
    }
}
