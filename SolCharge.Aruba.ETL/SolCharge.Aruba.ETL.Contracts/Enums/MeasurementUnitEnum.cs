﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"measurement_unit")]
    public enum MeasurementUnitEnum
    {
        [ProtoEnum(Name = @"METERS")]
        Meters = 0,
        [ProtoEnum(Name = @"FEET")]
        Feet = 1,
        [ProtoEnum(Name = @"PIXELS")]
        Pixels = 2,
    }
}
