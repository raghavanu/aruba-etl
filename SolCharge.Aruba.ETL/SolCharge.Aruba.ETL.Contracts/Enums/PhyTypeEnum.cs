﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"phy_type")]
    public enum PhyTypeEnum
    {
        [ProtoEnum(Name = @"PHY_TYPE_80211B")]
        PhyType80211b = 0,
        [ProtoEnum(Name = @"PHY_TYPE_80211A")]
        PhyType80211a = 1,
        [ProtoEnum(Name = @"PHY_TYPE_80211G")]
        PhyType80211g = 2,
        [ProtoEnum(Name = @"PHY_TYPE_80211AG")]
        PhyType80211ag = 3,
        [ProtoEnum(Name = @"PHY_TYPE_INVALID")]
        PhyTypeInvalid = 4,
    }
}
