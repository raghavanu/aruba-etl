﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"error_code")]
    public enum ErrorCodeEnum
    {
        [ProtoEnum(Name = @"ERROR_CODE_NO_ERROR")]
        ErrorCodeNoError = 0,
        [ProtoEnum(Name = @"ERROR_CODE_0_RSSI")]
        ErrorCode0Rssi = 1,
        [ProtoEnum(Name = @"ERROR_CODE_ONLY_1_RSSI")]
        ErrorCodeOnly1Rssi = 2,
        [ProtoEnum(Name = @"ERROR_CODE_ONLY_2_RSSI")]
        ErrorCodeOnly2Rssi = 3,
        [ProtoEnum(Name = @"ERROR_CODE_RSSI_QUALITY")]
        ErrorCodeRssiQuality = 4,
        [ProtoEnum(Name = @"ERROR_CODE_RSSI_OLD_TIMESTAMP")]
        ErrorCodeRssiOldTimestamp = 8,
        [ProtoEnum(Name = @"ERROR_CODE_RSSI_CLOSE_TIMESTAMP")]
        ErrorCodeRssiCloseTimestamp = 16,
        [ProtoEnum(Name = @"ERROR_CODE_LEGACY")]
        ErrorCodeLegacy = 1048575,
    }
}
