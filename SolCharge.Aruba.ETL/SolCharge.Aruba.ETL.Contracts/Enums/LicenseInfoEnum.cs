﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"license_info")]
    public enum LicenseInfoEnum
    {
        [ProtoEnum(Name = @"hb_Dhak")]
        hbDhak = 10,
        [ProtoEnum(Name = @"hb_LimitOk")]
        hbLimitOk = 11,
        [ProtoEnum(Name = @"hb_ThresholdXNotice")]
        hbThresholdXNotice = 20,
        [ProtoEnum(Name = @"hb_ThresholdOkNotice")]
        hbThresholdOkNotice = 21,
        [ProtoEnum(Name = @"hb_LicenseExceeded")]
        hbLicenseExceeded = 31,
        [ProtoEnum(Name = @"hb_EvalStarted")]
        hbEvalStarted = 41,
        [ProtoEnum(Name = @"hb_NewLimitExceeded")]
        hbNewLimitExceeded = 51,
        [ProtoEnum(Name = @"hb_EvalDone")]
        hbEvalDone = 61,
        [ProtoEnum(Name = @"hb_ALSOnline")]
        hbALSOnline = 71,
        [ProtoEnum(Name = @"hb_ALSDieing")]
        hbALSDieing = 81,
        [ProtoEnum(Name = @"hb_LICENSE_BLOCKED")]
        hbLICENSEBLOCKED = 91,
    }
}
