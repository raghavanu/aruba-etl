﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"data_prio")]
    public enum DataPrioEnum
    {
        [ProtoEnum(Name = @"DATA_PRIO_BK")]
        DataPrioBk = 0,
        [ProtoEnum(Name = @"DATA_PRIO_BE")]
        DataPrioBe = 1,
        [ProtoEnum(Name = @"DATA_PRIO_VI")]
        DataPrioVi = 2,
        [ProtoEnum(Name = @"DATA_PRIO_VO")]
        DataPrioVo = 3,
    }
}
