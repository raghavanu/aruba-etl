﻿using ProtoBuf;

namespace SolCharge.Aruba.ETL.Contracts.Enums
{
    [ProtoContract(Name = @"radio_mode")]
    public enum RadioModeEnum
    {
        [ProtoEnum(Name = @"RADIO_MODE_AP")]
        RadioModeAp = 0,
        [ProtoEnum(Name = @"RADIO_MODE_MESH_PORTAL")]
        RadioModeMeshPortal = 1,
        [ProtoEnum(Name = @"RADIO_MODE_MESH_POINT")]
        RadioModeMeshPoint = 2,
        [ProtoEnum(Name = @"RADIO_MODE_AIR_MONITOR")]
        RadioModeAirMonitor = 3,
        [ProtoEnum(Name = @"RADIO_MODE_SPECTRUM_SENSOR")]
        RadioModeSpectrumSensor = 4,
        [ProtoEnum(Name = @"RADIO_MODE_UNKNOWN")]
        RadioModeUnknown = 5,
    }
}
